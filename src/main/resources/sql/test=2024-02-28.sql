/*
 Navicat Premium Data Transfer

 Source Server         : local-mysql
 Source Server Type    : MySQL
 Source Server Version : 50741
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50741
 File Encoding         : 65001

 Date: 28/02/2024 17:03:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for a
-- ----------------------------
DROP TABLE IF EXISTS `a`;
CREATE TABLE `a`  (
  `event_id` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `page_id` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `action_time` bigint(20) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of a
-- ----------------------------
INSERT INTO `a` VALUES ('6228e67c', 'cb', 12423345345, 1);
INSERT INTO `a` VALUES ('5fba427b', 'aw', 457654645, 2);
INSERT INTO `a` VALUES ('222ec4ee', 'qe', 45645744, 3);
INSERT INTO `a` VALUES ('c03c0465', 'ty', 7685645, 4);

-- ----------------------------
-- Table structure for brm_user
-- ----------------------------
DROP TABLE IF EXISTS `brm_user`;
CREATE TABLE `brm_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '帐号',
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
  `login_salt` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盐',
  `go_id` bigint(20) NULL DEFAULT NULL COMMENT '组织ID',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父级账号ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色id',
  `logo` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'logo',
  `area_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国际代码',
  `phone_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公司',
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '城市',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ctime` bigint(20) NULL DEFAULT NULL COMMENT '注册时间',
  `utime` bigint(20) NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态（0：未激活；1：可用）',
  `is_del` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态（0：未删除;1:已删除）',
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `monitor_token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '监控TOKEN',
  `is_initialpass` tinyint(4) NULL DEFAULT 1 COMMENT '是否为初始化密码（0：否；1：是）',
  `emp_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `department_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门ID',
  `job` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `emp_status` tinyint(4) NULL DEFAULT 1 COMMENT '员工状态（0：离职；1：在职）',
  `business_card` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名片',
  `comein_uid` bigint(20) NULL DEFAULT NULL COMMENT '绑定进门账户ID',
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '职位',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 168 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic STATS_AUTO_RECALC = 0;

-- ----------------------------
-- Records of brm_user
-- ----------------------------
INSERT INTO `brm_user` VALUES (21, 'BRM000014', '', '', 101, NULL, 1, '', '+86', '17302640505', '', '', '', 1692774079134, 1693986068911, 1, 0, NULL, NULL, 1, '莫磊', '1', 'test-job', '', 1, '', 1530432, '');
INSERT INTO `brm_user` VALUES (23, 'BRM000016', 'fcb6d088228cad2440410a77feb45b82', 'vvaouf8lr5', 101, NULL, 1, NULL, '+86', '15526432374', NULL, NULL, NULL, 1692844922722, 1692844922722, 1, 0, NULL, NULL, 1, 'test-name-1', '1', 'test-job', NULL, 1, NULL, 101, NULL);
INSERT INTO `brm_user` VALUES (24, 'BRM000017', '751d22d6bd4d4cf356fc60d361216f2f', '6j9qq9rp9c', 101, NULL, 1, NULL, '+86', '17302640502', NULL, NULL, NULL, 1692844935802, 1692844935802, 1, 0, NULL, NULL, 1, 'test-name-2', '1', 'test-job', NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (25, 'BRM000018', '0217746f94982be9808a184f6196f2e1', 'sna9zc071i', 101, NULL, 1, NULL, '+86', '17302640503', NULL, NULL, NULL, 1692844944366, 1692844944366, 1, 0, NULL, NULL, 1, 'test-name-3', '1', 'test-job', NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (26, 'BRM000019', '8f9a36ffee351cca16b68e3767577a24', 'hrhd2y7ch5', 101, NULL, 1, NULL, '+86', '17302640504', NULL, NULL, NULL, 1692844955059, 1692844955059, 1, 0, NULL, NULL, 1, 'test-name-4', '1', 'test-job', NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (27, 'BRM000020', '2ca542bee954e19f3ca73d8d0bc6ad05', 'sia7qv42y7', 101, NULL, 1, NULL, '+86', '8888889601', NULL, NULL, NULL, 1692863212361, 1692863212361, 1, 0, NULL, NULL, 1, '张夏普', '1', NULL, '', 1, NULL, 1530402, NULL);
INSERT INTO `brm_user` VALUES (29, 'BRM000022', 'beee622d52d1f78c0b753437801af9b5', '6w1a0ts8cj', 101, NULL, 1, NULL, '+86', '15216153264', NULL, NULL, NULL, 1692863269921, 1692863269921, 0, 0, NULL, NULL, 1, '王均发', '1', NULL, '', 1, NULL, 1529488, NULL);
INSERT INTO `brm_user` VALUES (30, 'BRM000023', '', '', 101, NULL, 3, '', '+86', '15818436150', '', '', '', 1692866853858, 1692866853858, 0, 0, NULL, NULL, 1, '网', '1', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (31, 'BRM000024', '', '', 101, NULL, 3, '', '+86', '19090909999', '', '', '', 1692869192314, 1692869192314, 1, 0, NULL, NULL, 1, 'Rrrrrrr2', '1', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (32, 'BRM000025', '', '', 101, NULL, 3, '', '+86', '19090909998', '', '', '', 1692869911273, 1692869911273, 1, 0, NULL, NULL, 1, 'upd', '1', '开发', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (33, 'BRM000028', '', '', 101, NULL, 3, '', '+86', '15779003553', '', '', '', 1692943769936, 1692943769936, 1, 0, NULL, NULL, 1, '111', '1', '1', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (34, 'BRM000029', 'dacce005de5f608fb889cd35e22dc63e', 'npy6n0c8hi', 101, NULL, 1, 'img/2023-09-06/ca775faf-c7be-4d4a-b933-a9ab5f994561.png', '+86', '17346606277', NULL, NULL, NULL, 1692947856966, 1692947856966, 1, 0, NULL, NULL, 1, 'xixi', '1', 'tester', NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (35, 'BRM000030', 'c1369ee2b64b7b37bf17d949156c0cbc', 'qkrdk9d6bz', 101, NULL, 3, NULL, '+86', '13299999991', NULL, NULL, NULL, 1692947997833, 1692947997833, 1, 0, NULL, NULL, 1, 'lk', '1', 'tester', 'lk@gmail.com', 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (36, 'BRM000037', '8d7b751eaaacf65e09e6f8e9434f8310', '3y5s7jw77e', 1276, NULL, 1, NULL, 'demoData', 'demoData', NULL, NULL, NULL, 1692963675551, 1692963675551, 1, 0, NULL, NULL, 1, 'demoData', '34', 'demoData', 'demoData', 1, 'demoData', NULL, NULL);
INSERT INTO `brm_user` VALUES (37, 'BRM000039', '142546a322ec835fbc129af4bf27ca7a', 'zog5q7i56w', 1277, NULL, 1, NULL, 'demoData', 'demoData', NULL, NULL, NULL, 1692964178470, 1692964178470, 1, 0, NULL, NULL, 1, 'demoData', '36', 'demoData', 'demoData', 1, 'demoData', NULL, NULL);
INSERT INTO `brm_user` VALUES (38, 'BRM000040', '2d7d0282c2ea9fa76506cf2e49983fbc', '5vrd2ckcwa', 1278, NULL, 1, NULL, 'demoData', 'demoData', NULL, NULL, NULL, 1692964207928, 1692964207928, 1, 0, NULL, NULL, 1, 'demoData', '38', 'demoData', 'demoData', 1, 'demoData', NULL, NULL);
INSERT INTO `brm_user` VALUES (39, 'BRM000041', '', '', 101, NULL, 3, '', '+86', '15779003636', '', '', '', 1693187118022, 1693187118022, 1, 0, NULL, NULL, 1, 'test1', '1', '岗位', '', 1, '', NULL, '职位');
INSERT INTO `brm_user` VALUES (40, 'BRM000042', '83d85c1c7b59d8f2764d01fa0288f854', '5e5xhz3hws', 101, NULL, 3, NULL, '+86', '8888889602', NULL, NULL, NULL, 1693187600685, 1693187600685, 1, 0, NULL, NULL, 1, '龙波', '1', '', NULL, 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (41, 'BRM000047', '95545b907be8801fef1a19c1972e539b', 'tt4eg6pauv', 1284, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693207792127, 1693207792127, 1, 0, NULL, NULL, 1, '超管', '43', 'demoData', '121212@163.com', 1, 'https://testimage.comein.cn/comein-files/img/2019-08-19/df3bb01e-abde-4008-8411-413c9c0dc800/df3bb01e-abde-4008-8411-413c9c0dc800.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (42, 'BRM000048', '249f3105a78a94d81b74ca213918c28d', 'aa6by8j0sg', 1285, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693207816414, 1693207816414, 1, 0, NULL, NULL, 1, '超管', '45', 'demoData', '121212@163.com', 1, 'https://testimage.comein.cn/comein-files/img/2019-08-19/df3bb01e-abde-4008-8411-413c9c0dc800/df3bb01e-abde-4008-8411-413c9c0dc800.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (43, 'BRM000049', 'c46cae14f569fa419ad033cdb866478e', 'ee9v12gbmd', 1287, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693214292853, 1693214292853, 1, 0, NULL, NULL, 1, '超管', '47', 'demoData', '121212@163.com', 1, 'https://testimage.comein.cn/comein-files/img/2019-08-19/df3bb01e-abde-4008-8411-413c9c0dc800/df3bb01e-abde-4008-8411-413c9c0dc800.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (44, 'BRM000050', 'f68786acf90c99caec2c065d11c573a1', 'syrthy7d82', 1288, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693214354915, 1693214354915, 1, 0, NULL, NULL, 1, '超管', '49', 'demoData', '121212@163.com', 1, 'https://testimage.comein.cn/comein-files/img/2019-08-19/df3bb01e-abde-4008-8411-413c9c0dc800/df3bb01e-abde-4008-8411-413c9c0dc800.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (45, 'BRM000051', 'c8f95f56a50b4c972456e1963f69ac00', 'r5i8iyl9go', 1289, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693214423261, 1693214423261, 1, 0, NULL, NULL, 1, '超管', '51', 'demoData', '121212@163.com', 1, 'https://testimage.comein.cn/comein-files/img/2019-08-19/df3bb01e-abde-4008-8411-413c9c0dc800/df3bb01e-abde-4008-8411-413c9c0dc800.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (46, 'BRM000052', '39d26383b6cda8898ce17657d7fb6d84', 'lzykf6pwq5', 1290, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693217829533, 1693217829533, 1, 0, NULL, NULL, 1, '超管', '53', 'demoData', '121212@163.com', 1, 'https://testimage.comein.cn/comein-files/img/2019-08-19/df3bb01e-abde-4008-8411-413c9c0dc800/df3bb01e-abde-4008-8411-413c9c0dc800.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (47, 'BRM000053', 'c2751c25540b9b31cc6f1fbadf37c591', 'tdyvvk2rh6', 101, NULL, 1, NULL, '+86', '14776147137', NULL, NULL, NULL, 1693271865118, 1693271865118, 1, 0, NULL, NULL, 1, '陈鑫', '1', '', NULL, 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (49, 'BRM000054', '3bb5edb2e8d9832b49cceda0422a7b74', 'uhcbq64jkb', 101, NULL, 3, NULL, '+86', '15779003443', NULL, NULL, NULL, 1693278414051, 1693278414051, 1, 0, NULL, NULL, 1, 'leon', '1', '', NULL, 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (50, 'BRM000055', 'c57717a6c757a1a57fec18e8dec56e7a', 'ils35grho3', 1291, NULL, 1, NULL, '+86', '13569695858', NULL, NULL, NULL, 1693281343420, 1693281343420, 1, 0, NULL, NULL, 1, '超管姓名', '55', '运营', 'test@comein.cn', 1, 'https://testimage.comein.cn/comein-files/img/13b61462d411499f82fea82d94d2063c.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (51, 'BRM000056', '', '', 101, NULL, 3, '3.cn', '+86', '13227379708', '', '', '', 1693281569668, 1693281569668, 1, 0, NULL, NULL, 1, 'jcp', '1', 'job', '', 1, '', NULL, 'job');
INSERT INTO `brm_user` VALUES (52, 'BRM000057', '', '', 101, NULL, 3, '', '+86', '17302640509', '', '', '', 1693301335439, 1693301335439, 1, 0, NULL, NULL, 1, 'test-name-9', '1', 'test-job', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (53, 'BRM000058', '', '', 101, NULL, 3, '', '+86', '19925384855', '', '', '', 1693303015347, 1693303015347, 1, 0, NULL, NULL, 1, '陈玟树', '1', '', '', 1, '', 1530398, '');
INSERT INTO `brm_user` VALUES (54, 'BRM000059', 'eb1b615809beafec0e1eec3164a79534', 'v4b3cfu7tf', 1293, NULL, 1, NULL, '+86', '13569695858', NULL, NULL, NULL, 1693358190944, 1693358190944, 1, 0, NULL, NULL, 1, '超管姓名', '56', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (55, 'BRM000060', '9aaa1f041f275d6d6d20ef7321f7e0ce', 'brm', 1294, NULL, 1, NULL, '+86', '13569695851', NULL, NULL, NULL, 1693358616084, 1693472102578, 1, 0, NULL, NULL, 1, '进门测试0830', '58', '运营', 'test@comein.cn', 1, 'https://testimage.comein.cn/comein-files/img/fb944deb17ad4807b5c6c547f9a012c5.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (56, 'BRM000061', '', '', 101, NULL, 3, '', '+86', '152 1615 3264', '', '', '', 1693364911720, 1693364911720, 1, 0, NULL, NULL, 1, '王均发', '1', '', '', 1, '', 1530407, '');
INSERT INTO `brm_user` VALUES (57, 'BRM000062', '', '', 101, NULL, 3, '', '+86', '88888853333', '', '', '', 1693377555676, 1693377555676, 1, 0, NULL, NULL, 1, '晓丽', '1', '', '', 1, '', 1530442, '');
INSERT INTO `brm_user` VALUES (58, 'BRM000063', '', '', 101, NULL, 3, '', '+86', '15875518157', '', '', '', 1693383687667, 1693383687667, 1, 0, NULL, NULL, 1, '龙葵', '1', '', '', 1, '', 1517690, '');
INSERT INTO `brm_user` VALUES (59, 'BRM000064', '', '', 101, NULL, 3, '', '+86', '88888823333', '', '', '', 1693454451886, 1693454451886, 1, 0, NULL, NULL, 1, '小小丽', '1', '', '', 1, '', 1530449, '');
INSERT INTO `brm_user` VALUES (60, 'BRM000065', '313ed0cf567e13e954ab42dc1ded818b', 'ripv7f0ffv', 1300, NULL, 1, NULL, '+86', '18815425212', NULL, NULL, NULL, 1693466011326, 1693466735300, 1, 0, NULL, NULL, 1, '彭宇', '59', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (68, 'BRM000073', 'c992de206d6e43ab4c7b5b0384b9f4b6', 'alokzsdi3v', 1305, NULL, 1, NULL, '+86', '17302640505', NULL, NULL, NULL, 1693553175547, 1693553175547, 1, 0, NULL, NULL, 1, '测试超管-mol', '64', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (72, 'BRM000077', 'b8281d3436e937755b1a20f8ffddef11', 'ozqur0mu8m', 101, NULL, 3, NULL, '86', '18899990000', NULL, NULL, NULL, 1693635010215, 1693635010215, 1, 0, NULL, NULL, 1, '张柯荣', '1', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (73, 'BRM000078', '', '', 101, NULL, 2, '', '+86', '15768510451', '', '', '', 1693808828420, 1693984313252, 1, 0, NULL, NULL, 1, '刘洋', '1', '测试', '', 1, '', 1530480, '测试');
INSERT INTO `brm_user` VALUES (74, 'BRM000079', 'a0a98f81f57cb7759ec26ba9fa4979f7', '65hulewrmx', 101, NULL, 3, NULL, '+86', '18888887777', NULL, NULL, NULL, 1693809095375, 1693809095375, 0, 0, NULL, NULL, 1, '张翼德', '1', '', NULL, 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (75, 'BRM000080', '48e3821cca68ff10963b35049f584486', 'v9mficc2sv', 101, NULL, 3, NULL, '+86', '16677778888', NULL, NULL, NULL, 1693809315880, 1693809315880, 0, 0, NULL, NULL, 1, '无限拳', '1', '', NULL, 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (76, 'BRM000081', 'b12d4ceae9304d0314d2e815851089f0', '2xgmylv7a6', 101, NULL, 3, NULL, '+86', '17312309231', NULL, NULL, NULL, 1693809950435, 1693809950435, 0, 0, NULL, NULL, 1, '李建军', '1', 'Java开发工程师', NULL, 1, NULL, NULL, 'E1');
INSERT INTO `brm_user` VALUES (77, 'BRM000082', '52c0ce2fb9671ffc13069def172b537f', 't99em4szhw', 101, NULL, 2, NULL, '+86', '18719060050', NULL, NULL, NULL, 1693810737376, 1693810737376, 1, 0, NULL, NULL, 1, '李建军', '1', '', NULL, 1, NULL, 1530547, '');
INSERT INTO `brm_user` VALUES (78, 'BRM000083', '9e7f25261d0b699542aee32b4e4fb72a', '3mzz8dqnut', 1306, NULL, 1, NULL, '+86', '18818712542', NULL, NULL, NULL, 1693812471067, 1693812471067, 0, 0, NULL, NULL, 1, '彭宇', '76', 'dsds', 'pmhyul@163.com', 1, 'https://testimage.comein.cn/comein-files/img/d50b0decd6f14216b3e68fa8625d2e72.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (79, 'BRM000084', '', '', 101, NULL, 2, '', '+86', '88881234567', '', '', '', 1693812759321, 1693812759321, 1, 0, NULL, NULL, 1, '测试1', '1', '', '', 0, '', 1530467, '');
INSERT INTO `brm_user` VALUES (80, 'BRM000085', '52be78ff6ad9cf41ac70faf32ba60cf9', '92fvlfix2v', 101, NULL, 2, NULL, '+86', '88881234568', NULL, NULL, NULL, 1693812834743, 1693812834743, 1, 0, NULL, NULL, 1, '测试2', '1', '', NULL, 1, NULL, 1530475, '');
INSERT INTO `brm_user` VALUES (81, 'BRM000086', '', '', 101, NULL, 2, '', '+86', '13227379709', '', '', '', 1693812915538, 1693812915538, 0, 0, NULL, NULL, 1, '简超平', '1', 'dev', '', 1, '', NULL, 'job');
INSERT INTO `brm_user` VALUES (82, 'BRM000087', '1e50386e71bc8f8113118555b2c06162', 'r2cel8dz4u', 101, NULL, 2, NULL, '+86', '88881234569', NULL, NULL, NULL, 1693813384882, 1693813384882, 1, 0, NULL, NULL, 1, '测试3', '1', '', NULL, 1, NULL, 1530465, '');
INSERT INTO `brm_user` VALUES (83, 'BRM000088', '', '', 101, NULL, 2, '', '+86', '8888676756', '', '', '', 1693818057702, 1693818057702, 0, 0, NULL, NULL, 1, 'ikun', '1', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (84, 'BRM000089', '1cf641a95866d39f00ea88eceb15d581', 'j0umucf65y', 101, NULL, 2, NULL, '+86', '17137401256', NULL, NULL, NULL, 1693818213841, 1693818213841, 0, 0, NULL, NULL, 1, 'cxxxx', '1', '1', NULL, 1, NULL, NULL, '1');
INSERT INTO `brm_user` VALUES (85, 'BRM000090', '', '', 101, NULL, 2, '', '+86', '888866771', '', '', '', 1693818309334, 1693818309334, 0, 0, NULL, NULL, 1, 'ikun886', '1', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (86, 'BRM000091', '', '', 101, NULL, 3, '', '+86', '8888765', '', '', '', 1693818425526, 1693818425526, 0, 0, NULL, NULL, 1, 'ikun765', '1', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (87, 'BRM000092', '', '', 101, NULL, 3, '', '+86', '88880001', '', '', '', 1693818826479, 1693818826479, 0, 0, NULL, NULL, 0, 'ikun888881', '1', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (88, 'BRM000093', '09ee9c3d5ccfcbabd9f4d9f3b9c974ab', 'n6hkv9ot5i', 101, NULL, 2, NULL, '+86', '17137404560', NULL, NULL, NULL, 1693820088660, 1693820088660, 0, 0, NULL, NULL, 1, 'cxcccx', '1', '1', NULL, 1, NULL, NULL, '1');
INSERT INTO `brm_user` VALUES (89, 'BRM000094', 'b7a949a4edb6e90fbcab0e1136eca875', 'tkqrom7xrk', 101, NULL, 3, NULL, '+86', '17436606277', NULL, NULL, NULL, 1693877145302, 1693877145302, 0, 0, NULL, NULL, 1, 'test666', '1', '', '88@163.com', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (90, 'BRM000095', '', '', 1307, NULL, 3, '', '+86', '15768510450', '', '', '', 1693878637755, 1694050812581, 1, 0, NULL, NULL, 0, '刘洋', '131', '', '', 1, '', 1530469, '');
INSERT INTO `brm_user` VALUES (91, 'BRM000096', '', '', 101, NULL, 3, '', '+86', '88886711', '', '', '', 1693878742890, 1693878742890, 0, 0, NULL, NULL, 1, 'Ckun', '1', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (92, 'BRM000097', '', '', 101, NULL, 3, '', '+86', '8888999121', '', '', '', 1693878771109, 1693878771109, 0, 0, NULL, NULL, 1, 'bKUN', '1', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (93, 'BRM000098', '4e051a45e3bbcf28005a8d20182981d7', 'hm9uw77kvk', 1307, NULL, 2, NULL, '+86', '13928458102', NULL, NULL, NULL, 1693879134593, 1693879134593, 1, 0, NULL, NULL, 0, 'ldc', NULL, '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (94, 'BRM000099', '', '', 101, NULL, 2, '', '+86', '88881234510', '', '', '', 1693880548533, 1693880548533, 1, 0, NULL, NULL, 1, '测试4', '105', '开发', '123456@qq.com', 1, '', 1530474, '经理');
INSERT INTO `brm_user` VALUES (95, 'BRM000100', '', '', 101, NULL, 3, '', '+86', '13723232123', '', '', '', 1693881259536, 1693992073105, 0, 0, NULL, NULL, 1, 'bkunkun', '138', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (96, 'BRM000101', '34b3fc71289cf94a089671abef949b3f', 'kc0es6jlz6', 101, NULL, 3, NULL, '+86', '88888123121', NULL, NULL, NULL, 1693881283823, 1693881283823, 0, 0, NULL, NULL, 1, 'Bkundqw', '106', '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (97, 'BRM000102', '', '', 101, NULL, 3, '', '+86', '8888123121', '', '', '', 1693881355165, 1693881355165, 0, 0, NULL, NULL, 1, 'new-b1', '106', '', '', 0, '', NULL, '');
INSERT INTO `brm_user` VALUES (98, 'BRM000103', '', '', 101, NULL, 2, '', '+86', '88881234511', '', '', '', 1693881363049, 1693881363049, 0, 0, NULL, NULL, 1, '测试5', '1', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (99, 'BRM000104', '', '', 101, NULL, 3, '', '+86', '888812312311', '', '', '', 1693881444784, 1693881444784, 0, 0, NULL, NULL, 1, 'bsub1', '110', '', '', 0, '', NULL, '');
INSERT INTO `brm_user` VALUES (101, 'BRM000106', '', '', 101, NULL, 3, '', '+86', '17724638620', '', '', '', 1693894756032, 1693991959215, 0, 0, NULL, NULL, 1, '为规范的观点二个人的好地方天很符合发过火凤凰发过火凤凰好烦好烦好烦好烦好烦', '136', '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (102, 'BRM000107', '', '', 101, NULL, 2, '', '+86', '19520541916', '', '', '', 1693895626039, 1693992089545, 1, 0, NULL, NULL, 1, '刘洋11', '136', '', '', 1, '', 1530482, '');
INSERT INTO `brm_user` VALUES (103, 'BRM000108', '', '', 101, NULL, 3, '', '+86', '888812345123', '', '', '', 1693899455445, 1693899455445, 0, 0, NULL, NULL, 1, '测试6', NULL, '', '', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (104, 'BRM000109', 'd6e057172da1c99a4dfa893cd81bc92e', '3250vq502i', 101, NULL, 3, NULL, '+86', '888881092131212', NULL, NULL, NULL, 1693903130424, 1693903130424, 0, 0, NULL, NULL, 1, 'testnewdata', NULL, '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (106, 'BRM000111', 'b36bc8b7e897c08b94ea9267c4ea49c2', 'vxvcozc5bk', 1308, NULL, 3, NULL, '+86', '88887777987', NULL, NULL, NULL, 1693912988874, 1693912988874, 1, 0, NULL, NULL, 1, '莫磊测试用户', NULL, '', '', 1, NULL, 1530493, '');
INSERT INTO `brm_user` VALUES (107, 'BRM000112', '06cdc8a20d4d98316289b4b0c1b22c6a', 'pzpouv0ka6', 101, NULL, 3, NULL, '+86', '195201916', NULL, NULL, NULL, 1693913307833, 1693913307833, 0, 0, NULL, NULL, 1, '123', NULL, '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (108, 'BRM000113', '8d1f4cf8ea7d4f547749c94bbc038c80', 'et8n6k201k', 1308, NULL, 3, NULL, '+86', '888877777963', NULL, NULL, NULL, 1693914508812, 1693914508812, 1, 0, NULL, NULL, 1, '888877777963', NULL, '', '', 1, NULL, 1530495, '');
INSERT INTO `brm_user` VALUES (109, 'BRM000114', '59d2b5b17130494f385d9859a6b7cdc8', 'h9f04wlf0r', 1308, NULL, 3, NULL, '+86', '88887777796', NULL, NULL, NULL, 1693918047364, 1693918108370, 0, 0, NULL, NULL, 1, '测试国平', NULL, '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (110, 'BRM000115', '3f6d00edd2216b37875f33f00d5ac31c', 'yuoij1gkge', 1308, NULL, 3, NULL, '+86', '19800262310', NULL, NULL, NULL, 1693962625731, 1693962625731, 1, 0, NULL, NULL, 1, '陈小铭', NULL, NULL, '', 1, NULL, 1530489, NULL);
INSERT INTO `brm_user` VALUES (111, 'BRM000116', '', '', 1308, NULL, 3, '', '+86', '88881111222', '', '', '', 1693964140808, 1693967371911, 0, 0, NULL, NULL, 1, 'HX测试用户', '111', '', '', 0, '', NULL, '');
INSERT INTO `brm_user` VALUES (112, 'BRM000117', '06a62b1dc95dc69ff399643512997112', 'rrsua7sola', 1308, NULL, 3, NULL, '+86', '88882222333', NULL, NULL, NULL, 1693964217263, 1693964217263, 1, 0, NULL, NULL, 1, 'HX测试用户2', '111', '', '', 1, NULL, 1530501, '');
INSERT INTO `brm_user` VALUES (113, 'BRM000118', '', '', 1308, NULL, 3, '', '+86', '88883333333', '', '', '', 1693967597463, 1693967789055, 0, 0, NULL, NULL, 1, 'HX测试用户3', '111', '', '', 0, '', NULL, '');
INSERT INTO `brm_user` VALUES (114, 'BRM000119', '22332e4777635adc32bf53b57e2b2203', '50slecssh7', 1309, NULL, 1, NULL, '+86', '18666294656', NULL, NULL, NULL, 1693968355112, 1693990976271, 1, 0, NULL, NULL, 0, '杨泽勇', '113', '厉害的岗位', NULL, 1, 'https://testimage.comein.cn/comein-files/img/43f023b22d6d4afeb626608216a2d06d.jpg', 1530502, NULL);
INSERT INTO `brm_user` VALUES (115, 'BRM000120', '', '', 1310, NULL, 1, '', '+86', '17307553575', '', '', '', 1693969346500, 1694069622519, 1, 0, NULL, NULL, 1, 'lichanghe', '143', '', 'test123456@qq.com', 1, '', 1530505, '');
INSERT INTO `brm_user` VALUES (118, 'BRM000123', 'd62254819168bfdeb0c80e45fc9d8e05', '74xrx69uxv', 101, NULL, 3, NULL, '+86', '18782515095', NULL, NULL, NULL, 1693970844357, 1693970844357, 1, 0, NULL, NULL, 1, '123', NULL, '', '', 1, NULL, 1530506, '');
INSERT INTO `brm_user` VALUES (121, 'BRM000126', 'd0de587e8ab6cb333db49f45fd50faec', 'yfiv1vv22i', 1315, NULL, 1, NULL, '+86', '19800262310', NULL, NULL, NULL, 1693972430019, 1693972430019, 0, 0, NULL, NULL, 1, '铭先生', '119', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (122, 'BRM000127', '50e6a8b256f867d267776e5b6833b275', 'zmfytpxwm9', 1316, NULL, 1, NULL, '+86', '13163214301', NULL, NULL, NULL, 1693980993648, 1693980993648, 1, 0, NULL, NULL, 1, 'LL', '120', NULL, NULL, 1, NULL, 1530513, NULL);
INSERT INTO `brm_user` VALUES (123, 'BRM000128', '56c41e790ffa889fb2639ad61a576c9f', 'glm2tcss3e', 1317, NULL, 1, NULL, '+86', '18026970335', NULL, NULL, NULL, 1693982208533, 1693982208533, 1, 0, NULL, NULL, 1, '富国基金超管', '121', NULL, NULL, 1, NULL, 1530508, NULL);
INSERT INTO `brm_user` VALUES (124, 'BRM000129', 'efb97e681d0a846196f949bc916eeae3', '0tt79k20iv', 1318, NULL, 1, NULL, '+86', '15768510450', NULL, NULL, NULL, 1693983719348, 1693984718480, 1, 0, NULL, NULL, 1, '刘洋1', '123', '开发', '1234567@qq.com', 1, 'https://testimage.comein.cn/comein-files/img/fccbbd3f9ed94d7eb382becd0f6dd306.png', 1530509, NULL);
INSERT INTO `brm_user` VALUES (125, 'BRM000130', '771b83b6c3b8ef9eb1da940176c92727', 'acjnxxz87g', 1319, NULL, 1, NULL, '+86', '15818599883', NULL, NULL, NULL, 1693988146983, 1693988146983, 1, 0, NULL, NULL, 1, '卓一航', '125', '岗位', NULL, 1, 'https://testimage.comein.cn/comein-files/img/c3686178da9d4951a4783bd6ae6ea840.jpg', 1530530, NULL);
INSERT INTO `brm_user` VALUES (126, 'BRM000131', 'cac271b7b45558befff0d3ef4aa05f32', 'cgui3ss9ro', 1320, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1693988195055, 1693988195055, 0, 0, NULL, NULL, 1, '信先生', '126', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (127, 'BRM000132', '1c1d4332aed6179fce29b72a27743119', 'gu4k9r36ct', 1321, NULL, 1, NULL, '+86', '19520541916', NULL, NULL, NULL, 1693988338712, 1693988338712, 1, 0, NULL, NULL, 0, '刘洋2', '128', '开发', '1234567@qq.com', 1, 'https://testimage.comein.cn/comein-files/img/654c1c2b3cee4d03886b207e1a3b9371.png', 1530514, NULL);
INSERT INTO `brm_user` VALUES (128, 'BRM000133', '977fd49b6d683d3e5f5bc54b8deab9fb', '0p9pid5ref', 1322, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1693988489061, 1693995390395, 1, 0, NULL, NULL, 1, '上海科创超管', '129', NULL, NULL, 1, NULL, 1530523, NULL);
INSERT INTO `brm_user` VALUES (129, 'BRM000134', 'e8ec65f130cafe295164ec36b3884e43', '1xg1yq5xr0', 101, NULL, 3, NULL, '+86', '17782215122', NULL, NULL, NULL, 1693988913971, 1693988913971, 0, 0, NULL, NULL, 1, '测试12', NULL, '测试', '', 1, NULL, NULL, '测试');
INSERT INTO `brm_user` VALUES (130, 'BRM000135', '3a7a377226a0208e79390ec6e1bb6aa9', 'vtksloiydz', 1323, NULL, 1, NULL, '+86', '18782515095', NULL, NULL, NULL, 1693991761754, 1693994425069, 1, 0, NULL, NULL, 1, '重阳', '135', '测试', '554150183@qq.com', 1, 'https://testimage.comein.cn/comein-files/img/fc6faf2d8ed94b1da3bbece26a9fb193.png', 1530519, NULL);
INSERT INTO `brm_user` VALUES (131, 'BRM000136', '99ab221ac41db0dc8c2f14e9ebf0b1eb', '2aqtfywak3', 1324, NULL, 1, NULL, '+86', '19800262310', NULL, NULL, NULL, 1694049382123, 1694049382123, 1, 0, NULL, NULL, 1, '铭先生', '139', NULL, NULL, 1, NULL, 1530521, NULL);
INSERT INTO `brm_user` VALUES (132, 'BRM000137', '4dd15234a848ba427a5b1d35e9cb4cb3', '6jj44ago39', 1325, NULL, 1, NULL, '+86', '13163214301', NULL, NULL, NULL, 1694051928853, 1694068887860, 1, 0, NULL, NULL, 1, 'LL', '140', NULL, NULL, 1, NULL, 1530541, NULL);
INSERT INTO `brm_user` VALUES (133, 'BRM000138', '616960c3291679ef4f111bea8eebf6c6', '9hck9sfbfn', 1322, NULL, 3, NULL, '+86', '15875518157', NULL, NULL, NULL, 1694052507825, 1694052507825, 1, 0, NULL, NULL, 1, 'llb', NULL, '', '', 1, NULL, 1530525, '');
INSERT INTO `brm_user` VALUES (134, 'BRM000139', 'cb920bf494e852605a1c6eb33e6b16e9', 'x5mt5tfw4z', 1320, NULL, 3, NULL, '+86', '88881234555', NULL, NULL, NULL, 1694053856104, 1694053856104, 0, 0, NULL, NULL, 1, '88881234555', NULL, '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (135, 'BRM000140', 'c3bcecdee832caf28acd09d302d05632', 'sm2n5b9vsb', 1307, NULL, 2, NULL, '+86', '15779003443', NULL, NULL, NULL, 1694055004637, 1694055004637, 0, 0, NULL, NULL, 1, 'yyy', '100', '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (136, 'BRM000141', '33b54a02b7e5f632f89e839500babf13', 'd80xyxfsrr', 1320, NULL, 3, NULL, '+86', '15818436150', NULL, NULL, NULL, 1694055069323, 1694055069323, 1, 0, NULL, NULL, 1, 'LC', '126', '', '', 1, NULL, 1530551, '');
INSERT INTO `brm_user` VALUES (137, 'BRM000142', '89c611f36ae50b0d764cdfb08256ab12', 'z0a78qgm8a', 1320, NULL, 3, NULL, '+86', '88887878999', NULL, NULL, NULL, 1694055463126, 1694055463126, 1, 0, NULL, NULL, 1, '88887878999', '126', '', '', 1, NULL, 1530532, '');
INSERT INTO `brm_user` VALUES (138, 'BRM000143', '143069ac1c647c748bb5bbd78d932d32', 'j0yp2k4qur', 1320, NULL, 3, NULL, '+86', '88888787999', NULL, NULL, NULL, 1694056459236, 1694091477271, 1, 0, NULL, NULL, 1, '不不不', '126', '', '', 1, NULL, 1530536, '');
INSERT INTO `brm_user` VALUES (139, 'BRM000144', '2066dd496609a0032ab538f7ff6faf53', 'x0dpzxp7yu', 1320, NULL, 3, NULL, '+86', '17302640505', NULL, NULL, NULL, 1694056507111, 1694056507111, 1, 0, NULL, NULL, 1, 'molei', NULL, NULL, '', 1, NULL, 1530533, NULL);
INSERT INTO `brm_user` VALUES (140, 'BRM000145', '14de6e510fe9c29ea91cc01ee9685152', 'k7jzek9gl0', 1320, NULL, 3, NULL, '+86', '88881212999', NULL, NULL, NULL, 1694058477974, 1694058477974, 1, 0, NULL, NULL, 1, '88881212999', '126', '', '', 1, NULL, 1530538, '');
INSERT INTO `brm_user` VALUES (141, 'BRM000146', '', '', 1322, NULL, 3, '', '+86', '88881212999', '', '', '', 1694058993355, 1694070716019, 0, 0, NULL, NULL, 1, '88881212999', '129', '', '', 0, '', NULL, '');
INSERT INTO `brm_user` VALUES (142, 'BRM000147', '285a565407adb9e447969cf356dbf13f', 'i3epaoocux', 1326, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1694059121494, 1694067820085, 1, 0, NULL, NULL, 1, '中信超管', '144', NULL, NULL, 1, 'https://testimage.comein.cn/comein-files/img/64be29f06344476b93879ffa896b62c6.jpg', 1530542, NULL);
INSERT INTO `brm_user` VALUES (143, 'BRM000148', '41f9532fa487408c47a0068209e3f523', '91p6783bh1', 1327, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1694068045064, 1694068045064, 1, 0, NULL, NULL, 1, '东方超管', '145', NULL, NULL, 1, NULL, 1530548, NULL);
INSERT INTO `brm_user` VALUES (144, 'BRM000149', 'e848816e36a53d98185e838572aa77ca', 'f2pa2nptkd', 1307, NULL, 2, NULL, '+86', '88881234516', NULL, NULL, NULL, 1694068088963, 1694068088963, 1, 0, NULL, NULL, 1, '测试6', '100', '开发', '123@qq.com', 1, NULL, 1530544, '经理');
INSERT INTO `brm_user` VALUES (145, 'BRM000150', '', '', 1325, NULL, 3, '', '+86', '180269703351', '', '', '', 1694069057597, 1694136435743, 1, 0, NULL, NULL, 1, '11', '140', '', '', 1, '', 1530550, '');
INSERT INTO `brm_user` VALUES (146, 'BRM000151', '', '', 1310, NULL, 2, '', '+86', '13209873839', '', '', '', 1694069528201, 1694069615018, 0, 0, NULL, NULL, 1, '汪峰', '142', '', 'test132@163.com', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (147, 'BRM000152', '', '', 1310, NULL, 2, '', '+86', '13892839387', '', '', '', 1694069583415, 1694069608758, 0, 0, NULL, NULL, 1, '何乘风', '141', '', 'test112233@163.com', 1, '', NULL, '');
INSERT INTO `brm_user` VALUES (148, 'BRM000153', '72fbeff317d7d968ba9b96cb153cdeb1', 'zfeypje05p', 1328, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1694069615165, 1694075802742, 0, 0, NULL, NULL, 1, '长江超管', '151', 'fdfdf', 'pengyu@comein.cn', 1, 'https://testimage.comein.cn/comein-files/img/0b6a633191964427b2f37e134465d091.jpg', NULL, NULL);
INSERT INTO `brm_user` VALUES (149, 'BRM000154', '', '', 1310, NULL, 3, '', '+86', '13212839487', '', '', '', 1694069750593, 1694069771614, 0, 0, NULL, NULL, 1, '邵吉强', '142', '后勤', 'test123456789@qq.com', 1, '', NULL, 'W3');
INSERT INTO `brm_user` VALUES (150, 'BRM000155', 'e96c40fbbf99515cfbe015be41a13251', 'ngf8q1agwh', 1329, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1694072724355, 1694072724355, 1, 0, NULL, NULL, 1, '超管', '147', NULL, NULL, 1, NULL, 1530552, NULL);
INSERT INTO `brm_user` VALUES (151, 'BRM000156', '0c2fd204c046b275c46496f28a5f7a4c', '6rbj6a65s1', 1307, NULL, 2, NULL, '+86', '88881234517', NULL, NULL, NULL, 1694074419046, 1694074419046, 0, 0, NULL, NULL, 1, '测试7', '100', '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (152, 'BRM000157', '353c50b00bb0c99e77d5f4d9333a40ba', '9qbz5nffoe', 1307, NULL, 2, NULL, '+86', '88881234518', NULL, NULL, NULL, 1694074441465, 1694074441465, 0, 0, NULL, NULL, 1, '测试8', '100', '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (153, 'BRM000158', '95322654f3af2550a6790c449038a5d2', 'nccqdue40u', 1330, NULL, 1, NULL, '+86', '18666294656', NULL, NULL, NULL, 1694075276648, 1694075276648, 1, 0, NULL, NULL, 1, '杨泽勇', '153', '厉害的岗位', NULL, 1, 'https://testimage.comein.cn/comein-files/img/fc80a1937a654ace849f3290d783b222.jpg', 1530553, NULL);
INSERT INTO `brm_user` VALUES (154, 'BRM000159', '6b4ae16ea4c3ecebe62e348f6418ed9f', 'e60upq0m4z', 1328, NULL, 3, NULL, '+86', '88888787999', NULL, NULL, NULL, 1694078170521, 1694078170521, 1, 0, NULL, NULL, 1, '滚滚滚', '146', '', '', 1, NULL, 1530556, '');
INSERT INTO `brm_user` VALUES (155, 'BRM000160', '7f21b6f411be9db27a696fdb86f87581', 'm7novi3gcn', 101, NULL, 2, NULL, '+86', '15768510450', NULL, NULL, NULL, 1694078951512, 1694078951512, 0, 0, NULL, NULL, 1, '刘洋2', '1', '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (156, 'BRM000161', '479ff774949cfd2370ed380d7a8d5223', 'j3d7hclzia', 1331, NULL, 1, NULL, '+86', '15089205423', NULL, NULL, NULL, 1694079098283, 1694079187458, 0, 0, NULL, NULL, 1, '施素薇', '154', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (157, 'BRM000162', '065ec4ca54be980034ef2a03e1db7691', 'trt79gnmx5', 1305, NULL, NULL, NULL, '+86', '13501010101', NULL, NULL, NULL, 1694081427107, 1694081427107, 1, 0, NULL, NULL, 1, '纪要管理', NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (158, 'BRM000163', 'b4871a127da53b22971f243b8c015039', 'jzd910huyu', 1320, NULL, 3, NULL, '+86', '88881234222', NULL, NULL, NULL, 1694081508978, 1694086150476, 1, 0, NULL, NULL, 1, '哈哈哈', '126', '', '', 1, NULL, 1530558, '');
INSERT INTO `brm_user` VALUES (159, 'BRM000164', '4301d4f65456283abc5527c9aa1ea892', 'noe2ec8fow', 1329, NULL, 3, NULL, '+86', '88881234222', NULL, NULL, NULL, 1694085457207, 1694086251061, 1, 0, NULL, NULL, 1, '么么么么', NULL, NULL, '', 1, NULL, 1530560, NULL);
INSERT INTO `brm_user` VALUES (160, 'BRM000165', '958a34c3caed5233173d6bfdf4fcc4a1', 'gru2jyip6k', 1326, NULL, 3, NULL, '+86', '88881234222', NULL, NULL, NULL, 1694087560068, 1694087593193, 1, 0, NULL, NULL, 1, '快快快', NULL, NULL, '', 1, NULL, 1530561, NULL);
INSERT INTO `brm_user` VALUES (161, 'BRM000166', '', '', 1327, NULL, 3, '', '+86', '88881234222', '', '', '', 1694088326084, 1694088384950, 1, 0, NULL, NULL, 1, '88881234222', '145', '', '', 1, '', 1530564, '');
INSERT INTO `brm_user` VALUES (162, 'BRM000167', 'ca46879c429edabd532623e1e123815f', 'ixmmu7rq6g', 1320, NULL, 3, NULL, '+86', '19925384855', NULL, NULL, NULL, 1694088628592, 1694088628592, 1, 0, NULL, NULL, 1, '真实的名字', NULL, NULL, '', 1, NULL, 1530562, NULL);
INSERT INTO `brm_user` VALUES (163, 'BRM000168', '69f65964751046dd7c807538f0b5a239', 'mga2sn3gly', 1327, NULL, NULL, NULL, '+86', NULL, NULL, NULL, NULL, 1694137266793, 1694137266793, 1, 0, NULL, NULL, 1, NULL, NULL, '', '', 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (164, 'brm000169', '6f5ca3af9a75718d47ed23c0096d77c9', '85dturjw9c', 1332, NULL, 1, NULL, '+86', '17302640505', NULL, NULL, NULL, 1694139240556, 1694139240556, 0, 0, NULL, NULL, 1, 'molei', '155', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `brm_user` VALUES (165, 'BRM000170', '4ca23875886e5fd523349a458ab3d1c6', 'n5u3e37bed', 1332, NULL, 3, NULL, '+86', '88889999789', NULL, NULL, NULL, 1694139681376, 1694139681376, 0, 0, NULL, NULL, 1, 'ceshi', '155', '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (166, 'BRM000171', 'd675c30ce613288b3f54beb2a4275de6', 'ntmps108ko', 1332, NULL, 2, NULL, '+86', '18026970335', NULL, NULL, NULL, 1694140058749, 1694140058749, 0, 0, NULL, NULL, 1, '18026970335', '155', '', '', 1, NULL, NULL, '');
INSERT INTO `brm_user` VALUES (167, 'BRM000172', 'ed5e9cb2e394d9e4c1faec9a5580269b', '2mvmrz6t45', 13234, NULL, NULL, NULL, '+86', '18782515095', NULL, NULL, NULL, 1694140135424, 1694140135424, 1, 0, NULL, NULL, 1, '重阳', '156', NULL, '554150183@qq.com', 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for brm_user_two
-- ----------------------------
DROP TABLE IF EXISTS `brm_user_two`;
CREATE TABLE `brm_user_two`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '帐号',
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
  `login_salt` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盐',
  `go_id` bigint(20) NULL DEFAULT NULL COMMENT '组织ID',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父级账号ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色id',
  `logo` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'logo',
  `area_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国际代码',
  `phone_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公司',
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '城市',
  `remark` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ctime` bigint(20) NULL DEFAULT NULL COMMENT '注册时间',
  `utime` bigint(20) NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态（0：未激活；1：可用）',
  `is_del` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态（0：未删除;1:已删除）',
  `token` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `monitor_token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '监控TOKEN',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 168 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic STATS_AUTO_RECALC = 0;

-- ----------------------------
-- Records of brm_user_two
-- ----------------------------
INSERT INTO `brm_user_two` VALUES (21, 'BRM000014', '', '', 101, NULL, 1, '', '+86', '17302640505', '', '', '', 1692774079134, 1693986068911, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (23, 'BRM000016', 'fcb6d088228cad2440410a77feb45b82', 'vvaouf8lr5', 101, NULL, 1, NULL, '+86', '15526432374', NULL, NULL, NULL, 1692844922722, 1692844922722, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (24, 'BRM000017', '751d22d6bd4d4cf356fc60d361216f2f', '6j9qq9rp9c', 101, NULL, 1, NULL, '+86', '17302640502', NULL, NULL, NULL, 1692844935802, 1692844935802, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (25, 'BRM000018', '0217746f94982be9808a184f6196f2e1', 'sna9zc071i', 101, NULL, 1, NULL, '+86', '17302640503', NULL, NULL, NULL, 1692844944366, 1692844944366, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (26, 'BRM000019', '8f9a36ffee351cca16b68e3767577a24', 'hrhd2y7ch5', 101, NULL, 1, NULL, '+86', '17302640504', NULL, NULL, NULL, 1692844955059, 1692844955059, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (27, 'BRM000020', '2ca542bee954e19f3ca73d8d0bc6ad05', 'sia7qv42y7', 101, NULL, 1, NULL, '+86', '8888889601', NULL, NULL, NULL, 1692863212361, 1692863212361, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (29, 'BRM000022', 'beee622d52d1f78c0b753437801af9b5', '6w1a0ts8cj', 101, NULL, 1, NULL, '+86', '15216153264', NULL, NULL, NULL, 1692863269921, 1692863269921, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (30, 'BRM000023', '', '', 101, NULL, 3, '', '+86', '15818436150', '', '', '', 1692866853858, 1692866853858, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (31, 'BRM000024', '', '', 101, NULL, 3, '', '+86', '19090909999', '', '', '', 1692869192314, 1692869192314, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (32, 'BRM000025', '', '', 101, NULL, 3, '', '+86', '19090909998', '', '', '', 1692869911273, 1692869911273, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (33, 'BRM000028', '', '', 101, NULL, 3, '', '+86', '15779003553', '', '', '', 1692943769936, 1692943769936, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (34, 'BRM000029', 'dacce005de5f608fb889cd35e22dc63e', 'npy6n0c8hi', 101, NULL, 1, 'img/2023-09-06/ca775faf-c7be-4d4a-b933-a9ab5f994561.png', '+86', '17346606277', NULL, NULL, NULL, 1692947856966, 1692947856966, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (35, 'BRM000030', 'c1369ee2b64b7b37bf17d949156c0cbc', 'qkrdk9d6bz', 101, NULL, 3, NULL, '+86', '13299999991', NULL, NULL, NULL, 1692947997833, 1692947997833, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (36, 'BRM000037', '8d7b751eaaacf65e09e6f8e9434f8310', '3y5s7jw77e', 1276, NULL, 1, NULL, 'demoData', 'demoData', NULL, NULL, NULL, 1692963675551, 1692963675551, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (37, 'BRM000039', '142546a322ec835fbc129af4bf27ca7a', 'zog5q7i56w', 1277, NULL, 1, NULL, 'demoData', 'demoData', NULL, NULL, NULL, 1692964178470, 1692964178470, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (38, 'BRM000040', '2d7d0282c2ea9fa76506cf2e49983fbc', '5vrd2ckcwa', 1278, NULL, 1, NULL, 'demoData', 'demoData', NULL, NULL, NULL, 1692964207928, 1692964207928, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (39, 'BRM000041', '', '', 101, NULL, 3, '', '+86', '15779003636', '', '', '', 1693187118022, 1693187118022, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (40, 'BRM000042', '83d85c1c7b59d8f2764d01fa0288f854', '5e5xhz3hws', 101, NULL, 3, NULL, '+86', '8888889602', NULL, NULL, NULL, 1693187600685, 1693187600685, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (41, 'BRM000047', '95545b907be8801fef1a19c1972e539b', 'tt4eg6pauv', 1284, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693207792127, 1693207792127, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (42, 'BRM000048', '249f3105a78a94d81b74ca213918c28d', 'aa6by8j0sg', 1285, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693207816414, 1693207816414, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (43, 'BRM000049', 'c46cae14f569fa419ad033cdb866478e', 'ee9v12gbmd', 1287, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693214292853, 1693214292853, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (44, 'BRM000050', 'f68786acf90c99caec2c065d11c573a1', 'syrthy7d82', 1288, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693214354915, 1693214354915, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (45, 'BRM000051', 'c8f95f56a50b4c972456e1963f69ac00', 'r5i8iyl9go', 1289, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693214423261, 1693214423261, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (46, 'BRM000052', '39d26383b6cda8898ce17657d7fb6d84', 'lzykf6pwq5', 1290, NULL, 1, NULL, '+86', '18815452526', NULL, NULL, NULL, 1693217829533, 1693217829533, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (47, 'BRM000053', 'c2751c25540b9b31cc6f1fbadf37c591', 'tdyvvk2rh6', 101, NULL, 1, NULL, '+86', '14776147137', NULL, NULL, NULL, 1693271865118, 1693271865118, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (49, 'BRM000054', '3bb5edb2e8d9832b49cceda0422a7b74', 'uhcbq64jkb', 101, NULL, 3, NULL, '+86', '15779003443', NULL, NULL, NULL, 1693278414051, 1693278414051, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (50, 'BRM000055', 'c57717a6c757a1a57fec18e8dec56e7a', 'ils35grho3', 1291, NULL, 1, NULL, '+86', '13569695858', NULL, NULL, NULL, 1693281343420, 1693281343420, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (51, 'BRM000056', '', '', 101, NULL, 3, '3.cn', '+86', '13227379708', '', '', '', 1693281569668, 1693281569668, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (52, 'BRM000057', '', '', 101, NULL, 3, '', '+86', '17302640509', '', '', '', 1693301335439, 1693301335439, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (53, 'BRM000058', '', '', 101, NULL, 3, '', '+86', '19925384855', '', '', '', 1693303015347, 1693303015347, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (54, 'BRM000059', 'eb1b615809beafec0e1eec3164a79534', 'v4b3cfu7tf', 1293, NULL, 1, NULL, '+86', '13569695858', NULL, NULL, NULL, 1693358190944, 1693358190944, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (55, 'BRM000060', '9aaa1f041f275d6d6d20ef7321f7e0ce', 'brm', 1294, NULL, 1, NULL, '+86', '13569695851', NULL, NULL, NULL, 1693358616084, 1693472102578, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (56, 'BRM000061', '', '', 101, NULL, 3, '', '+86', '152 1615 3264', '', '', '', 1693364911720, 1693364911720, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (57, 'BRM000062', '', '', 101, NULL, 3, '', '+86', '88888853333', '', '', '', 1693377555676, 1693377555676, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (58, 'BRM000063', '', '', 101, NULL, 3, '', '+86', '15875518157', '', '', '', 1693383687667, 1693383687667, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (59, 'BRM000064', '', '', 101, NULL, 3, '', '+86', '88888823333', '', '', '', 1693454451886, 1693454451886, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (60, 'BRM000065', '313ed0cf567e13e954ab42dc1ded818b', 'ripv7f0ffv', 1300, NULL, 1, NULL, '+86', '18815425212', NULL, NULL, NULL, 1693466011326, 1693466735300, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (68, 'BRM000073', 'c992de206d6e43ab4c7b5b0384b9f4b6', 'alokzsdi3v', 1305, NULL, 1, NULL, '+86', '17302640505', NULL, NULL, NULL, 1693553175547, 1693553175547, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (72, 'BRM000077', 'b8281d3436e937755b1a20f8ffddef11', 'ozqur0mu8m', 101, NULL, 3, NULL, '86', '18899990000', NULL, NULL, NULL, 1693635010215, 1693635010215, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (73, 'BRM000078', '', '', 101, NULL, 2, '', '+86', '15768510451', '', '', '', 1693808828420, 1693984313252, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (74, 'BRM000079', 'a0a98f81f57cb7759ec26ba9fa4979f7', '65hulewrmx', 101, NULL, 3, NULL, '+86', '18888887777', NULL, NULL, NULL, 1693809095375, 1693809095375, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (75, 'BRM000080', '48e3821cca68ff10963b35049f584486', 'v9mficc2sv', 101, NULL, 3, NULL, '+86', '16677778888', NULL, NULL, NULL, 1693809315880, 1693809315880, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (76, 'BRM000081', 'b12d4ceae9304d0314d2e815851089f0', '2xgmylv7a6', 101, NULL, 3, NULL, '+86', '17312309231', NULL, NULL, NULL, 1693809950435, 1693809950435, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (77, 'BRM000082', '52c0ce2fb9671ffc13069def172b537f', 't99em4szhw', 101, NULL, 2, NULL, '+86', '18719060050', NULL, NULL, NULL, 1693810737376, 1693810737376, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (78, 'BRM000083', '9e7f25261d0b699542aee32b4e4fb72a', '3mzz8dqnut', 1306, NULL, 1, NULL, '+86', '18818712542', NULL, NULL, NULL, 1693812471067, 1693812471067, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (79, 'BRM000084', '', '', 101, NULL, 2, '', '+86', '88881234567', '', '', '', 1693812759321, 1693812759321, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (80, 'BRM000085', '52be78ff6ad9cf41ac70faf32ba60cf9', '92fvlfix2v', 101, NULL, 2, NULL, '+86', '88881234568', NULL, NULL, NULL, 1693812834743, 1693812834743, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (81, 'BRM000086', '', '', 101, NULL, 2, '', '+86', '13227379709', '', '', '', 1693812915538, 1693812915538, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (82, 'BRM000087', '1e50386e71bc8f8113118555b2c06162', 'r2cel8dz4u', 101, NULL, 2, NULL, '+86', '88881234569', NULL, NULL, NULL, 1693813384882, 1693813384882, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (83, 'BRM000088', '', '', 101, NULL, 2, '', '+86', '8888676756', '', '', '', 1693818057702, 1693818057702, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (84, 'BRM000089', '1cf641a95866d39f00ea88eceb15d581', 'j0umucf65y', 101, NULL, 2, NULL, '+86', '17137401256', NULL, NULL, NULL, 1693818213841, 1693818213841, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (85, 'BRM000090', '', '', 101, NULL, 2, '', '+86', '888866771', '', '', '', 1693818309334, 1693818309334, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (86, 'BRM000091', '', '', 101, NULL, 3, '', '+86', '8888765', '', '', '', 1693818425526, 1693818425526, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (87, 'BRM000092', '', '', 101, NULL, 3, '', '+86', '88880001', '', '', '', 1693818826479, 1693818826479, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (88, 'BRM000093', '09ee9c3d5ccfcbabd9f4d9f3b9c974ab', 'n6hkv9ot5i', 101, NULL, 2, NULL, '+86', '17137404560', NULL, NULL, NULL, 1693820088660, 1693820088660, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (89, 'BRM000094', 'b7a949a4edb6e90fbcab0e1136eca875', 'tkqrom7xrk', 101, NULL, 3, NULL, '+86', '17436606277', NULL, NULL, NULL, 1693877145302, 1693877145302, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (90, 'BRM000095', '', '', 1307, NULL, 3, '', '+86', '15768510450', '', '', '', 1693878637755, 1694050812581, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (91, 'BRM000096', '', '', 101, NULL, 3, '', '+86', '88886711', '', '', '', 1693878742890, 1693878742890, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (92, 'BRM000097', '', '', 101, NULL, 3, '', '+86', '8888999121', '', '', '', 1693878771109, 1693878771109, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (93, 'BRM000098', '4e051a45e3bbcf28005a8d20182981d7', 'hm9uw77kvk', 1307, NULL, 2, NULL, '+86', '13928458102', NULL, NULL, NULL, 1693879134593, 1693879134593, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (94, 'BRM000099', '', '', 101, NULL, 2, '', '+86', '88881234510', '', '', '', 1693880548533, 1693880548533, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (95, 'BRM000100', '', '', 101, NULL, 3, '', '+86', '13723232123', '', '', '', 1693881259536, 1693992073105, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (96, 'BRM000101', '34b3fc71289cf94a089671abef949b3f', 'kc0es6jlz6', 101, NULL, 3, NULL, '+86', '88888123121', NULL, NULL, NULL, 1693881283823, 1693881283823, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (97, 'BRM000102', '', '', 101, NULL, 3, '', '+86', '8888123121', '', '', '', 1693881355165, 1693881355165, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (98, 'BRM000103', '', '', 101, NULL, 2, '', '+86', '88881234511', '', '', '', 1693881363049, 1693881363049, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (99, 'BRM000104', '', '', 101, NULL, 3, '', '+86', '888812312311', '', '', '', 1693881444784, 1693881444784, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (101, 'BRM000106', '', '', 101, NULL, 3, '', '+86', '17724638620', '', '', '', 1693894756032, 1693991959215, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (102, 'BRM000107', '', '', 101, NULL, 2, '', '+86', '19520541916', '', '', '', 1693895626039, 1693992089545, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (103, 'BRM000108', '', '', 101, NULL, 3, '', '+86', '888812345123', '', '', '', 1693899455445, 1693899455445, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (104, 'BRM000109', 'd6e057172da1c99a4dfa893cd81bc92e', '3250vq502i', 101, NULL, 3, NULL, '+86', '888881092131212', NULL, NULL, NULL, 1693903130424, 1693903130424, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (106, 'BRM000111', 'b36bc8b7e897c08b94ea9267c4ea49c2', 'vxvcozc5bk', 1308, NULL, 3, NULL, '+86', '88887777987', NULL, NULL, NULL, 1693912988874, 1693912988874, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (107, 'BRM000112', '06cdc8a20d4d98316289b4b0c1b22c6a', 'pzpouv0ka6', 101, NULL, 3, NULL, '+86', '195201916', NULL, NULL, NULL, 1693913307833, 1693913307833, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (108, 'BRM000113', '8d1f4cf8ea7d4f547749c94bbc038c80', 'et8n6k201k', 1308, NULL, 3, NULL, '+86', '888877777963', NULL, NULL, NULL, 1693914508812, 1693914508812, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (109, 'BRM000114', '59d2b5b17130494f385d9859a6b7cdc8', 'h9f04wlf0r', 1308, NULL, 3, NULL, '+86', '88887777796', NULL, NULL, NULL, 1693918047364, 1693918108370, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (110, 'BRM000115', '3f6d00edd2216b37875f33f00d5ac31c', 'yuoij1gkge', 1308, NULL, 3, NULL, '+86', '19800262310', NULL, NULL, NULL, 1693962625731, 1693962625731, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (111, 'BRM000116', '', '', 1308, NULL, 3, '', '+86', '88881111222', '', '', '', 1693964140808, 1693967371911, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (112, 'BRM000117', '06a62b1dc95dc69ff399643512997112', 'rrsua7sola', 1308, NULL, 3, NULL, '+86', '88882222333', NULL, NULL, NULL, 1693964217263, 1693964217263, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (113, 'BRM000118', '', '', 1308, NULL, 3, '', '+86', '88883333333', '', '', '', 1693967597463, 1693967789055, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (114, 'BRM000119', '22332e4777635adc32bf53b57e2b2203', '50slecssh7', 1309, NULL, 1, NULL, '+86', '18666294656', NULL, NULL, NULL, 1693968355112, 1693990976271, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (115, 'BRM000120', '', '', 1310, NULL, 1, '', '+86', '17307553575', '', '', '', 1693969346500, 1694069622519, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (118, 'BRM000123', 'd62254819168bfdeb0c80e45fc9d8e05', '74xrx69uxv', 101, NULL, 3, NULL, '+86', '18782515095', NULL, NULL, NULL, 1693970844357, 1693970844357, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (121, 'BRM000126', 'd0de587e8ab6cb333db49f45fd50faec', 'yfiv1vv22i', 1315, NULL, 1, NULL, '+86', '19800262310', NULL, NULL, NULL, 1693972430019, 1693972430019, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (122, 'BRM000127', '50e6a8b256f867d267776e5b6833b275', 'zmfytpxwm9', 1316, NULL, 1, NULL, '+86', '13163214301', NULL, NULL, NULL, 1693980993648, 1693980993648, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (123, 'BRM000128', '56c41e790ffa889fb2639ad61a576c9f', 'glm2tcss3e', 1317, NULL, 1, NULL, '+86', '18026970335', NULL, NULL, NULL, 1693982208533, 1693982208533, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (124, 'BRM000129', 'efb97e681d0a846196f949bc916eeae3', '0tt79k20iv', 1318, NULL, 1, NULL, '+86', '15768510450', NULL, NULL, NULL, 1693983719348, 1693984718480, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (125, 'BRM000130', '771b83b6c3b8ef9eb1da940176c92727', 'acjnxxz87g', 1319, NULL, 1, NULL, '+86', '15818599883', NULL, NULL, NULL, 1693988146983, 1693988146983, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (126, 'BRM000131', 'cac271b7b45558befff0d3ef4aa05f32', 'cgui3ss9ro', 1320, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1693988195055, 1693988195055, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (127, 'BRM000132', '1c1d4332aed6179fce29b72a27743119', 'gu4k9r36ct', 1321, NULL, 1, NULL, '+86', '19520541916', NULL, NULL, NULL, 1693988338712, 1693988338712, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (128, 'BRM000133', '977fd49b6d683d3e5f5bc54b8deab9fb', '0p9pid5ref', 1322, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1693988489061, 1693995390395, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (129, 'BRM000134', 'e8ec65f130cafe295164ec36b3884e43', '1xg1yq5xr0', 101, NULL, 3, NULL, '+86', '17782215122', NULL, NULL, NULL, 1693988913971, 1693988913971, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (130, 'BRM000135', '3a7a377226a0208e79390ec6e1bb6aa9', 'vtksloiydz', 1323, NULL, 1, NULL, '+86', '18782515095', NULL, NULL, NULL, 1693991761754, 1693994425069, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (131, 'BRM000136', '99ab221ac41db0dc8c2f14e9ebf0b1eb', '2aqtfywak3', 1324, NULL, 1, NULL, '+86', '19800262310', NULL, NULL, NULL, 1694049382123, 1694049382123, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (132, 'BRM000137', '4dd15234a848ba427a5b1d35e9cb4cb3', '6jj44ago39', 1325, NULL, 1, NULL, '+86', '13163214301', NULL, NULL, NULL, 1694051928853, 1694068887860, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (133, 'BRM000138', '616960c3291679ef4f111bea8eebf6c6', '9hck9sfbfn', 1322, NULL, 3, NULL, '+86', '15875518157', NULL, NULL, NULL, 1694052507825, 1694052507825, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (134, 'BRM000139', 'cb920bf494e852605a1c6eb33e6b16e9', 'x5mt5tfw4z', 1320, NULL, 3, NULL, '+86', '88881234555', NULL, NULL, NULL, 1694053856104, 1694053856104, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (135, 'BRM000140', 'c3bcecdee832caf28acd09d302d05632', 'sm2n5b9vsb', 1307, NULL, 2, NULL, '+86', '15779003443', NULL, NULL, NULL, 1694055004637, 1694055004637, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (136, 'BRM000141', '33b54a02b7e5f632f89e839500babf13', 'd80xyxfsrr', 1320, NULL, 3, NULL, '+86', '15818436150', NULL, NULL, NULL, 1694055069323, 1694055069323, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (137, 'BRM000142', '89c611f36ae50b0d764cdfb08256ab12', 'z0a78qgm8a', 1320, NULL, 3, NULL, '+86', '88887878999', NULL, NULL, NULL, 1694055463126, 1694055463126, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (138, 'BRM000143', '143069ac1c647c748bb5bbd78d932d32', 'j0yp2k4qur', 1320, NULL, 3, NULL, '+86', '88888787999', NULL, NULL, NULL, 1694056459236, 1694091477271, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (139, 'BRM000144', '2066dd496609a0032ab538f7ff6faf53', 'x0dpzxp7yu', 1320, NULL, 3, NULL, '+86', '17302640505', NULL, NULL, NULL, 1694056507111, 1694056507111, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (140, 'BRM000145', '14de6e510fe9c29ea91cc01ee9685152', 'k7jzek9gl0', 1320, NULL, 3, NULL, '+86', '88881212999', NULL, NULL, NULL, 1694058477974, 1694058477974, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (141, 'BRM000146', '', '', 1322, NULL, 3, '', '+86', '88881212999', '', '', '', 1694058993355, 1694070716019, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (142, 'BRM000147', '285a565407adb9e447969cf356dbf13f', 'i3epaoocux', 1326, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1694059121494, 1694067820085, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (143, 'BRM000148', '41f9532fa487408c47a0068209e3f523', '91p6783bh1', 1327, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1694068045064, 1694068045064, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (144, 'BRM000149', 'e848816e36a53d98185e838572aa77ca', 'f2pa2nptkd', 1307, NULL, 2, NULL, '+86', '88881234516', NULL, NULL, NULL, 1694068088963, 1694068088963, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (145, 'BRM000150', '', '', 1325, NULL, 3, '', '+86', '180269703351', '', '', '', 1694069057597, 1694136435743, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (146, 'BRM000151', '', '', 1310, NULL, 2, '', '+86', '13209873839', '', '', '', 1694069528201, 1694069615018, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (147, 'BRM000152', '', '', 1310, NULL, 2, '', '+86', '13892839387', '', '', '', 1694069583415, 1694069608758, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (148, 'BRM000153', '72fbeff317d7d968ba9b96cb153cdeb1', 'zfeypje05p', 1328, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1694069615165, 1694075802742, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (149, 'BRM000154', '', '', 1310, NULL, 3, '', '+86', '13212839487', '', '', '', 1694069750593, 1694069771614, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (150, 'BRM000155', 'e96c40fbbf99515cfbe015be41a13251', 'ngf8q1agwh', 1329, NULL, 1, NULL, '+86', '13265597876', NULL, NULL, NULL, 1694072724355, 1694072724355, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (151, 'BRM000156', '0c2fd204c046b275c46496f28a5f7a4c', '6rbj6a65s1', 1307, NULL, 2, NULL, '+86', '88881234517', NULL, NULL, NULL, 1694074419046, 1694074419046, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (152, 'BRM000157', '353c50b00bb0c99e77d5f4d9333a40ba', '9qbz5nffoe', 1307, NULL, 2, NULL, '+86', '88881234518', NULL, NULL, NULL, 1694074441465, 1694074441465, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (153, 'BRM000158', '95322654f3af2550a6790c449038a5d2', 'nccqdue40u', 1330, NULL, 1, NULL, '+86', '18666294656', NULL, NULL, NULL, 1694075276648, 1694075276648, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (154, 'BRM000159', '6b4ae16ea4c3ecebe62e348f6418ed9f', 'e60upq0m4z', 1328, NULL, 3, NULL, '+86', '88888787999', NULL, NULL, NULL, 1694078170521, 1694078170521, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (155, 'BRM000160', '7f21b6f411be9db27a696fdb86f87581', 'm7novi3gcn', 101, NULL, 2, NULL, '+86', '15768510450', NULL, NULL, NULL, 1694078951512, 1694078951512, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (156, 'BRM000161', '479ff774949cfd2370ed380d7a8d5223', 'j3d7hclzia', 1331, NULL, 1, NULL, '+86', '15089205423', NULL, NULL, NULL, 1694079098283, 1694079187458, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (157, 'BRM000162', '065ec4ca54be980034ef2a03e1db7691', 'trt79gnmx5', 1305, NULL, NULL, NULL, '+86', '13501010101', NULL, NULL, NULL, 1694081427107, 1694081427107, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (158, 'BRM000163', 'b4871a127da53b22971f243b8c015039', 'jzd910huyu', 1320, NULL, 3, NULL, '+86', '88881234222', NULL, NULL, NULL, 1694081508978, 1694086150476, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (159, 'BRM000164', '4301d4f65456283abc5527c9aa1ea892', 'noe2ec8fow', 1329, NULL, 3, NULL, '+86', '88881234222', NULL, NULL, NULL, 1694085457207, 1694086251061, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (160, 'BRM000165', '958a34c3caed5233173d6bfdf4fcc4a1', 'gru2jyip6k', 1326, NULL, 3, NULL, '+86', '88881234222', NULL, NULL, NULL, 1694087560068, 1694087593193, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (161, 'BRM000166', '', '', 1327, NULL, 3, '', '+86', '88881234222', '', '', '', 1694088326084, 1694088384950, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (162, 'BRM000167', 'ca46879c429edabd532623e1e123815f', 'ixmmu7rq6g', 1320, NULL, 3, NULL, '+86', '19925384855', NULL, NULL, NULL, 1694088628592, 1694088628592, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (163, 'BRM000168', '69f65964751046dd7c807538f0b5a239', 'mga2sn3gly', 1327, NULL, NULL, NULL, '+86', NULL, NULL, NULL, NULL, 1694137266793, 1694137266793, 1, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (164, 'brm000169', '6f5ca3af9a75718d47ed23c0096d77c9', '85dturjw9c', 1332, NULL, 1, NULL, '+86', '17302640505', NULL, NULL, NULL, 1694139240556, 1694139240556, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (165, 'BRM000170', '4ca23875886e5fd523349a458ab3d1c6', 'n5u3e37bed', 1332, NULL, 3, NULL, '+86', '88889999789', NULL, NULL, NULL, 1694139681376, 1694139681376, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (166, 'BRM000171', 'd675c30ce613288b3f54beb2a4275de6', 'ntmps108ko', 1332, NULL, 2, NULL, '+86', '18026970335', NULL, NULL, NULL, 1694140058749, 1694140058749, 0, 0, NULL, NULL);
INSERT INTO `brm_user_two` VALUES (167, 'BRM000172', 'ed5e9cb2e394d9e4c1faec9a5580269b', '2mvmrz6t45', 13234, NULL, NULL, NULL, '+86', '18782515095', NULL, NULL, NULL, 1694140135424, 1694140135424, 1, 0, NULL, NULL);

-- ----------------------------
-- Table structure for dim_user_info
-- ----------------------------
DROP TABLE IF EXISTS `dim_user_info`;
CREATE TABLE `dim_user_info`  (
  `dt` date NULL DEFAULT NULL COMMENT '分区时间',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户Id',
  `member_level` int(11) NULL DEFAULT NULL COMMENT '等级',
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户等级表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dim_user_info
-- ----------------------------
INSERT INTO `dim_user_info` VALUES ('2024-01-16', 1, 1, 'jack');
INSERT INTO `dim_user_info` VALUES ('2024-01-16', 2, 2, 'tom');
INSERT INTO `dim_user_info` VALUES ('2024-01-16', 3, 3, 'pig');
INSERT INTO `dim_user_info` VALUES ('2024-01-16', 4, 4, 'jip');
INSERT INTO `dim_user_info` VALUES ('2024-01-17', 1, 4, 'jack');
INSERT INTO `dim_user_info` VALUES ('2024-01-17', 2, 3, 'tom');
INSERT INTO `dim_user_info` VALUES ('2024-01-17', 3, 2, 'pig');
INSERT INTO `dim_user_info` VALUES ('2024-01-17', 4, 1, 'jip');

-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp`  (
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `birthday` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES ('ying', 30, '2020-09-05 20:06:39');
INSERT INTO `emp` VALUES ('hei', 10, '2020-09-05 20:06:39');
INSERT INTO `emp` VALUES ('bai', 20, '2020-09-05 20:06:39');
INSERT INTO `emp` VALUES ('bai', 20, '2020-09-05 20:07:17');
INSERT INTO `emp` VALUES ('hei', 10, '2020-09-05 20:07:17');
INSERT INTO `emp` VALUES ('ying', 30, '2020-09-05 20:07:17');

-- ----------------------------
-- Table structure for pvuv_sink
-- ----------------------------
DROP TABLE IF EXISTS `pvuv_sink`;
CREATE TABLE `pvuv_sink`  (
  `dt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pv` bigint(20) NULL DEFAULT NULL,
  `uv` bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `password` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, 'zhisheng1', 'password1', 19);
INSERT INTO `student` VALUES (2, 'zhisheng2', 'password2', 20);
INSERT INTO `student` VALUES (3, 'zhisheng3', 'password3', 21);
INSERT INTO `student` VALUES (4, 'zhisheng4', 'password4', 22);
INSERT INTO `student` VALUES (5, 'zhisheng5', 'password5', 23);
INSERT INTO `student` VALUES (6, 'zhisheng6', 'password6', 24);
INSERT INTO `student` VALUES (7, 'zhisheng7', 'password7', 25);
INSERT INTO `student` VALUES (8, 'zhisheng8', 'password8', 26);
INSERT INTO `student` VALUES (9, 'zhisheng9', 'password9', 27);
INSERT INTO `student` VALUES (10, 'zhisheng10', 'password10', 28);
INSERT INTO `student` VALUES (11, 'zhisheng11', 'password11', 29);
INSERT INTO `student` VALUES (12, 'zhisheng12', 'password12', 30);
INSERT INTO `student` VALUES (13, 'zhisheng13', 'password13', 31);
INSERT INTO `student` VALUES (14, 'zhisheng14', 'password14', 32);
INSERT INTO `student` VALUES (15, 'zhisheng15', 'password15', 33);
INSERT INTO `student` VALUES (16, 'zhisheng16', 'password16', 34);
INSERT INTO `student` VALUES (17, 'zhisheng17', 'password17', 35);
INSERT INTO `student` VALUES (18, 'zhisheng18', 'password18', 36);
INSERT INTO `student` VALUES (19, 'zhisheng19', 'password19', 37);
INSERT INTO `student` VALUES (20, 'zhisheng20', 'password20', 38);
INSERT INTO `student` VALUES (21, 'zhisheng21', 'password21', 39);
INSERT INTO `student` VALUES (22, 'zhisheng22', 'password22', 40);
INSERT INTO `student` VALUES (23, 'zhisheng23', 'password23', 41);
INSERT INTO `student` VALUES (24, 'zhisheng24', 'password24', 42);
INSERT INTO `student` VALUES (25, 'zhisheng25', 'password25', 43);
INSERT INTO `student` VALUES (26, 'zhisheng26', 'password26', 44);
INSERT INTO `student` VALUES (27, 'zhisheng27', 'password27', 45);
INSERT INTO `student` VALUES (28, 'zhisheng28', 'password28', 46);
INSERT INTO `student` VALUES (29, 'zhisheng29', 'password29', 47);
INSERT INTO `student` VALUES (30, 'zhisheng30', 'password30', 48);
INSERT INTO `student` VALUES (31, 'zhisheng31', 'password31', 49);
INSERT INTO `student` VALUES (32, 'zhisheng32', 'password32', 50);
INSERT INTO `student` VALUES (33, 'zhisheng33', 'password33', 51);
INSERT INTO `student` VALUES (34, 'zhisheng34', 'password34', 52);
INSERT INTO `student` VALUES (35, 'zhisheng35', 'password35', 53);
INSERT INTO `student` VALUES (36, 'zhisheng36', 'password36', 54);
INSERT INTO `student` VALUES (37, 'zhisheng37', 'password37', 55);
INSERT INTO `student` VALUES (38, 'zhisheng38', 'password38', 56);
INSERT INTO `student` VALUES (39, 'zhisheng39', 'password39', 57);
INSERT INTO `student` VALUES (40, 'zhisheng40', 'password40', 58);
INSERT INTO `student` VALUES (41, 'zhisheng41', 'password41', 59);
INSERT INTO `student` VALUES (42, 'zhisheng42', 'password42', 60);
INSERT INTO `student` VALUES (43, 'zhisheng43', 'password43', 61);
INSERT INTO `student` VALUES (44, 'zhisheng44', 'password44', 62);
INSERT INTO `student` VALUES (45, 'zhisheng45', 'password45', 63);
INSERT INTO `student` VALUES (46, 'zhisheng46', 'password46', 64);
INSERT INTO `student` VALUES (47, 'zhisheng47', 'password47', 65);
INSERT INTO `student` VALUES (48, 'zhisheng48', 'password48', 66);
INSERT INTO `student` VALUES (49, 'zhisheng49', 'password49', 67);
INSERT INTO `student` VALUES (50, 'zhisheng50', 'password50', 68);
INSERT INTO `student` VALUES (51, 'zhisheng51', 'password51', 69);
INSERT INTO `student` VALUES (52, 'zhisheng52', 'password52', 70);
INSERT INTO `student` VALUES (53, 'zhisheng53', 'password53', 71);
INSERT INTO `student` VALUES (54, 'zhisheng54', 'password54', 72);
INSERT INTO `student` VALUES (55, 'zhisheng55', 'password55', 73);
INSERT INTO `student` VALUES (56, 'zhisheng56', 'password56', 74);
INSERT INTO `student` VALUES (57, 'zhisheng57', 'password57', 75);
INSERT INTO `student` VALUES (58, 'zhisheng58', 'password58', 76);
INSERT INTO `student` VALUES (59, 'zhisheng59', 'password59', 77);
INSERT INTO `student` VALUES (60, 'zhisheng60', 'password60', 78);
INSERT INTO `student` VALUES (61, 'zhisheng61', 'password61', 79);
INSERT INTO `student` VALUES (62, 'zhisheng62', 'password62', 80);
INSERT INTO `student` VALUES (63, 'zhisheng63', 'password63', 81);
INSERT INTO `student` VALUES (64, 'zhisheng64', 'password64', 82);
INSERT INTO `student` VALUES (65, 'zhisheng65', 'password65', 83);
INSERT INTO `student` VALUES (66, 'zhisheng66', 'password66', 84);
INSERT INTO `student` VALUES (67, 'zhisheng67', 'password67', 85);
INSERT INTO `student` VALUES (68, 'zhisheng68', 'password68', 86);
INSERT INTO `student` VALUES (69, 'zhisheng69', 'password69', 87);
INSERT INTO `student` VALUES (70, 'zhisheng70', 'password70', 88);
INSERT INTO `student` VALUES (71, 'zhisheng71', 'password71', 89);
INSERT INTO `student` VALUES (72, 'zhisheng72', 'password72', 90);
INSERT INTO `student` VALUES (73, 'zhisheng73', 'password73', 91);
INSERT INTO `student` VALUES (74, 'zhisheng74', 'password74', 92);
INSERT INTO `student` VALUES (75, 'zhisheng75', 'password75', 93);
INSERT INTO `student` VALUES (76, 'zhisheng76', 'password76', 94);
INSERT INTO `student` VALUES (77, 'zhisheng77', 'password77', 95);
INSERT INTO `student` VALUES (78, 'zhisheng78', 'password78', 96);
INSERT INTO `student` VALUES (79, 'zhisheng79', 'password79', 97);
INSERT INTO `student` VALUES (80, 'zhisheng80', 'password80', 98);
INSERT INTO `student` VALUES (81, 'zhisheng81', 'password81', 99);
INSERT INTO `student` VALUES (82, 'zhisheng82', 'password82', 100);
INSERT INTO `student` VALUES (83, 'zhisheng83', 'password83', 101);
INSERT INTO `student` VALUES (84, 'zhisheng84', 'password84', 102);
INSERT INTO `student` VALUES (85, 'zhisheng85', 'password85', 103);
INSERT INTO `student` VALUES (86, 'zhisheng86', 'password86', 104);
INSERT INTO `student` VALUES (87, 'zhisheng87', 'password87', 105);
INSERT INTO `student` VALUES (88, 'zhisheng88', 'password88', 106);
INSERT INTO `student` VALUES (89, 'zhisheng89', 'password89', 107);
INSERT INTO `student` VALUES (90, 'zhisheng90', 'password90', 108);
INSERT INTO `student` VALUES (91, 'zhisheng91', 'password91', 109);
INSERT INTO `student` VALUES (92, 'zhisheng92', 'password92', 110);
INSERT INTO `student` VALUES (93, 'zhisheng93', 'password93', 111);
INSERT INTO `student` VALUES (94, 'zhisheng94', 'password94', 112);
INSERT INTO `student` VALUES (95, 'zhisheng95', 'password95', 113);
INSERT INTO `student` VALUES (96, 'zhisheng96', 'password96', 114);
INSERT INTO `student` VALUES (97, 'zhisheng97', 'password97', 115);
INSERT INTO `student` VALUES (98, 'zhisheng98', 'password98', 116);
INSERT INTO `student` VALUES (99, 'zhisheng99', 'password99', 117);
INSERT INTO `student` VALUES (100, 'zhisheng100', 'password100', 118);

-- ----------------------------
-- Table structure for t_num_test
-- ----------------------------
DROP TABLE IF EXISTS `t_num_test`;
CREATE TABLE `t_num_test`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `salary` double NULL DEFAULT NULL,
  `cash` double NULL DEFAULT NULL,
  `real_money` decimal(15, 3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_num_test
-- ----------------------------
INSERT INTO `t_num_test` VALUES (1, 'Jack', 12, 35.467, 52.546, 321.835);
INSERT INTO `t_num_test` VALUES (2, 'Tom', 25, 89.123, 352.546, 99989.835);

-- ----------------------------
-- Table structure for t_raytek_result
-- ----------------------------
DROP TABLE IF EXISTS `t_raytek_result`;
CREATE TABLE `t_raytek_result`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `temperature` float NULL DEFAULT NULL,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `timestamp` timestamp(0) NULL DEFAULT NULL,
  `location` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_test
-- ----------------------------
DROP TABLE IF EXISTS `t_test`;
CREATE TABLE `t_test`  (
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `total` int(11) NULL DEFAULT NULL,
  `insert_time` timestamp(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_test
-- ----------------------------
INSERT INTO `t_test` VALUES ('aaa', 9, '2020-08-06 00:19:20');
INSERT INTO `t_test` VALUES ('LLL', 3, '2020-08-06 00:19:20');
INSERT INTO `t_test` VALUES ('HHH', 3, '2020-08-06 00:19:20');
INSERT INTO `t_test` VALUES ('KKK', 3, '2020-08-06 00:19:20');
INSERT INTO `t_test` VALUES ('CCC', 6, '2020-08-06 00:19:20');
INSERT INTO `t_test` VALUES ('bbb', 6, '2020-08-06 00:19:20');
INSERT INTO `t_test` VALUES ('aaa', 3, '2020-08-11 22:39:55');
INSERT INTO `t_test` VALUES ('LLL', 1, '2020-08-11 22:39:55');
INSERT INTO `t_test` VALUES ('HHH', 1, '2020-08-11 22:39:55');
INSERT INTO `t_test` VALUES ('KKK', 1, '2020-08-11 22:39:55');
INSERT INTO `t_test` VALUES ('CCC', 2, '2020-08-11 22:39:55');
INSERT INTO `t_test` VALUES ('bbb', 2, '2020-08-11 22:39:55');
INSERT INTO `t_test` VALUES ('RRR', 2, '2020-08-11 22:47:00');
INSERT INTO `t_test` VALUES ('MMM', 3, '2020-08-11 22:47:00');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `gender` int(11) NULL DEFAULT NULL,
  `address` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, 'jack', 25, 0, 'beijing');
INSERT INTO `t_user` VALUES (2, 'tom', 26, 1, 'shengzhen');
INSERT INTO `t_user` VALUES (3, 'tom', 26, 1, 'shengzhen');
INSERT INTO `t_user` VALUES (4, 'jack', 25, 0, 'beijing');
INSERT INTO `t_user` VALUES (5, 'jack', 25, 0, 'beijing');
INSERT INTO `t_user` VALUES (6, 'jack', 25, 0, 'beijing');
INSERT INTO `t_user` VALUES (7, 'mock', 23, 1, 'shengzhen');
INSERT INTO `t_user` VALUES (8, 'solar22', 54, 0, 'shangdong');
INSERT INTO `t_user` VALUES (9, 'kkk', 66, 1, 'KKK');
INSERT INTO `t_user` VALUES (10, 'QQQQQ', 77, 0, 'HHHHHH');
INSERT INTO `t_user` VALUES (11, 'AAAA', 88, 1, 'WWWW');
INSERT INTO `t_user` VALUES (12, 'BBB', 99, 0, 'YYYYY');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `created` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'aaa', '123', '2020-08-06 00:19:20');
INSERT INTO `tb_user` VALUES (2, 'bbb', '456', '2020-08-07 22:26:36');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `balance` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'alanchan', 'sink mysql', 'alan.chan.chn@163.com', 19, 800);
INSERT INTO `user` VALUES (2, 'alanchanchn', 'vx', 'alan.chan.chn@163.com', 19, 800);

-- ----------------------------
-- Table structure for ws
-- ----------------------------
DROP TABLE IF EXISTS `ws`;
CREATE TABLE `ws`  (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ts` bigint(20) NULL DEFAULT NULL,
  `vc` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ws
-- ----------------------------
INSERT INTO `ws` VALUES ('jack', 12, 1511658000);
INSERT INTO `ws` VALUES ('tom', 12, 1511658000);

SET FOREIGN_KEY_CHECKS = 1;
