package com.jie.flink.cdc.util;

import java.lang.reflect.Type;

public interface BeanConvertor {
    /**
     * 序列化
     *
     * @param t
     * @param <T>
     * @return
     */
    <T> String serialize(T t) throws Exception;

    /**
     * 反序列化
     *
     * @param responseBody
     * @param classOfT
     * @param <T>
     * @return
     */
    <T> T deserialize(String responseBody, Class<T> classOfT) throws Exception;

    /**
     * 反序列化
     *
     * @param responseBody
     * @param type
     * @param <T>
     * @return
     */
    <T> T deserialize(String responseBody, Type type) throws Exception;
}
