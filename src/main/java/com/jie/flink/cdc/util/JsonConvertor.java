package com.jie.flink.cdc.util;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public interface JsonConvertor  extends BeanConvertor {

    /**
     * 反序列化到数组
     * @param json
     * @param <T>
     * @return
     */
    <T> List<T> deserializeToArray(String json, Class<T> classOfT) throws Exception;



    /**
     * 反序列化到数组
     * @param json
     * @param <T>
     * @return
     */
    <T> List<T> deserializeToArray(String json,Type type) throws Exception;
    /**
     * 转换到Map
     *
     * @param t
     * @param <T>
     * @return
     */
    <T> Map<String,Object> convertToMap(T t) throws Exception;

    /**
     * 转换到Map
     * @param json
     * @return
     * @throws Exception
     */
    Map<String,Object> convertToMap(String json) throws Exception;

    /**
     * map转换到bean
     *
     * @param map
     * @param classOfT
     * @param <T>
     * @return
     */
    <T> T convertToBean(Map<String,Object> map, Class<T> classOfT) throws Exception;

    /**
     * map转换到bean
     *
     * @param map
     * @param type
     * @param <T>
     * @return
     */
    <T> T convertToBean(Map<String,Object> map, Type type) throws Exception;

}
