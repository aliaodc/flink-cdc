package com.jie.flink.cdc.util;

import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author zhanggj
 * @date 2023/5/25 11:01
 * @desc
 */
public class FileUtil {

    /**
     * 读取文件
     * @return
     * @throws IOException
     */
    public static String readAllBytes(String fileName) throws Exception {
        StringBuilder result  = new StringBuilder();
        File file = ResourceUtils.getFile("classpath:".concat(fileName));

        if (!file.exists() || !file.isFile()) {
            return null;
        }

        FileInputStream fileInputStream = new FileInputStream(file);
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String data = null;
        while((data = bufferedReader.readLine()) != null) {
            result.append(data);
        }
        return result.toString();
    }
}
