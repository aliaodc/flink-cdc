/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jie.flink.cdc.rocketmq.flink.function;

import org.apache.commons.lang3.Validate;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.rocketmq.common.message.Message;

public class SinkMapFunction extends ProcessFunction<String, Message> {

    private String topic;

    private String tag;

    public SinkMapFunction() {}

    public SinkMapFunction(String topic, String tag) {
        this.topic = topic;
        this.tag = tag;
    }

    @Override
    public void processElement(String data, Context ctx, Collector<Message> out)
            throws Exception {
        Validate.notNull(topic, "the message topic is null");
        Validate.notNull(data, "the message body is null");

        Message message = new Message(topic, tag, data.getBytes());
        out.collect(message);
    }
}
