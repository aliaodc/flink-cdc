package com.jie.flink.cdc.flinksink.config;

import com.jie.flink.cdc.flinksink.KafkaFlinkSinkBuilder;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

/**
 * @author zhanggj
 * @date 2023/3/1 15:34
 * @desc
 */
@Data
@Configuration
@ConditionalOnProperty(value = "jie.flink-cdc.stream.sink.kafka.enabled")
@ConfigurationProperties(prefix = "jie.flink-cdc.stream.sink.kafka")
public class KafkaConfigProperties extends SinkConfigProperties {
    public KafkaConfigProperties() {
        key = UUID.randomUUID().toString();
        sinkMap.put(key,  KafkaFlinkSinkBuilder.getInstance());
    }
    private String bootstrapServer;
    private String topic;
}
