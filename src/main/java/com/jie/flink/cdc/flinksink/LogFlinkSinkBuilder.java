package com.jie.flink.cdc.flinksink;

import com.jie.flink.cdc.flinksink.config.SinkLogConfigProperties;
import org.apache.flink.api.connector.sink2.Sink;

/**
 * @author zhanggj
 * @date 2023/5/29 10:22
 * @desc
 */
public class LogFlinkSinkBuilder implements FlinkSinkBuilder<SinkLogConfigProperties> {
    private LogFlinkSinkBuilder() {
    }

    private static class InstanceHolder{
        private static LogFlinkSinkBuilder INSTANCE = new LogFlinkSinkBuilder();
    }
    public static LogFlinkSinkBuilder getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public Sink<String> buildSink(final SinkLogConfigProperties sinkConfigProperties) {
        return new FlinkLogSink();
    }
}
