package com.jie.flink.cdc.flinksink.config;

import com.jie.flink.cdc.flinksink.EsFlinkSinkBuilder;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

/**
 * @author zhanggj
 * @date 2023/3/1 15:34
 * @desc
 * https://blog.csdn.net/u010772882/article/details/125363121
 */
@Data
@Configuration
@ConditionalOnProperty(value = "jie.flink-cdc.stream.sink.es.enabled")
@ConfigurationProperties(prefix = "jie.flink-cdc.stream.sink.es")
public class ElasticsearchConfigProperties extends SinkConfigProperties {
    public ElasticsearchConfigProperties(){
        key = UUID.randomUUID().toString();
        sinkMap.put(key, EsFlinkSinkBuilder.getInstance());
    }
    private String[] hosts;
    private String userName;
    private String password;
}
