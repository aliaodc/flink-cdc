package com.jie.flink.cdc.flinksink;

import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.connector.sink2.SinkWriter;

import java.io.IOException;

/**
 * @author zhanggj
 * @date 2023/3/22 20:53
 * @desc
 */
@Slf4j
public class FlinkLogSinkWriter implements SinkWriter<String> {
    @Override
    public void write(final String element, final Context context) throws IOException, InterruptedException {
        log.info("收到变更数据：{}", element);
    }

    @Override
    public void flush(final boolean endOfInput) throws IOException, InterruptedException {

    }

    @Override
    public void close() throws Exception {

    }
}
