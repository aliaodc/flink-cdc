package com.jie.flink.cdc.flinksink.config;

import com.jie.flink.cdc.doman.FlinkCdcSinkType;
import com.jie.flink.cdc.flinksink.FlinkSinkBuilder;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhanggj
 * @date 2023/5/25 11:19
 * @desc
 */
@Data
public class SinkConfigProperties {
    public static final Map<String , FlinkSinkBuilder> sinkMap = new HashMap<>();

    protected String key;

    private String sinkName;

    private FlinkCdcSinkType sinkType;

}
