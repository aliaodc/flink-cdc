package com.jie.flink.cdc.flinksink;

import com.jie.flink.cdc.flinksink.config.SinkConfigProperties;
import org.apache.flink.api.connector.sink2.Sink;

/**
 * @author zhanggj
 * @date 2023/5/29 10:16
 * @desc
 */
public interface FlinkSinkBuilder<T extends SinkConfigProperties> {
    /**
     * 构造sink
     * @param sinkConfigProperties
     * @return
     */
    Sink<String> buildSink(T sinkConfigProperties);
}
