package com.jie.flink.cdc.flinksink.config;

import com.jie.flink.cdc.flinksink.LogFlinkSinkBuilder;
import com.jie.flink.cdc.util.JsonUtils;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

/**
 * @author zhanggj
 * @date 2023/3/22 20:57
 * @desc
 */
@Data
@Configuration
@ConditionalOnProperty(value = "jie.flink-cdc.stream.sink.log.enabled")
@ConfigurationProperties(prefix = "jie.flink-cdc.stream.sink.log")
public class SinkLogConfigProperties extends SinkConfigProperties {
    public SinkLogConfigProperties() {
        key = UUID.randomUUID().toString();
        sinkMap.put(key, LogFlinkSinkBuilder.getInstance());
    }

}
