package com.jie.flink.cdc.flinksink;

import org.apache.flink.api.connector.sink2.Sink;
import org.apache.flink.api.connector.sink2.SinkWriter;

import java.io.IOException;

/**
 * @author zhanggj
 * @date 2023/3/22 20:51
 * @desc
 */
public class FlinkLogSink implements Sink<String> {
    @Override
    public SinkWriter<String> createWriter(final InitContext context) throws IOException {
        return new FlinkLogSinkWriter();
    }
}
