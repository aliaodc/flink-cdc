package com.jie.flink.cdc.datafilter;

import com.jie.flink.cdc.doman.DataChangeInfo;

import java.io.Serializable;

/**
 * @author zhanggj
 * @data 2023/2/10
 * pg 数据过滤器
 */
public interface DataFilter extends Serializable {

    default boolean filterOut(DataChangeInfo dataChangeInfo) {
        return false;
    }
}
