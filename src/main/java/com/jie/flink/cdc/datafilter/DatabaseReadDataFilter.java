package com.jie.flink.cdc.datafilter;

import com.jie.flink.cdc.doman.DataChangeInfo;
import com.jie.flink.cdc.util.LogPrintLimitUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author zhanggj
 * @data 2023/2/10
 * 数据库读数据顾虑器
 */
@Slf4j
public class DatabaseReadDataFilter implements DataFilter {
    private static final long serialVersionUID = 34729347392846248L;

    /**
     * 采集数据类型 扫描读
     */
    private static final String DATA_CHANGE_TYPE_READ = "read";

    @Override
    public boolean filterOut(DataChangeInfo dataChangeInfo) {
        if (DATA_CHANGE_TYPE_READ.equals(dataChangeInfo.getEventType())) {
            LogPrintLimitUtil.printDelayLog(log, String.format("忽略初始化读数据的采集"));
            return true;
        }
        return false;
    }
}
