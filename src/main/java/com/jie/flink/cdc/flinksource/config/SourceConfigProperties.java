package com.jie.flink.cdc.flinksource.config;

import com.jie.flink.cdc.doman.FlinkCdcSourceType;
import com.jie.flink.cdc.service.FlinkCdcService;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhanggj
 * @date 2023/5/25 14:18
 * @desc
 */
@Data
public class SourceConfigProperties {

    public static final Map<String, FlinkCdcService> sourceMap = new HashMap<>();

    protected String key;

    private FlinkCdcSourceType sourceType;

    /**
     * 数据库hostname
     */
    private String hostName;

    /**
     * 数据库 端口
     */
    private Integer port;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 是否忽略初始化扫描数据
     */
    private Boolean initReadIgnore;
}
