package com.jie.flink.cdc.flinksource;

import com.jie.flink.cdc.datafilter.DataFilter;
import com.jie.flink.cdc.doman.DataChangeInfo;
import com.jie.flink.cdc.flinksource.config.SourceConfigWashMapper;
import com.jie.flink.cdc.util.JsonUtils;
import com.jie.flink.cdc.util.LogPrintLimitUtil;
import com.ververica.cdc.debezium.DebeziumDeserializationSchema;
import io.debezium.data.Envelope;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.util.Collector;
import org.apache.kafka.connect.data.Field;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.source.SourceRecord;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author zhanggj
 * @data 2023/1/31
 * 数据转换
 */
@Slf4j
public class PostgreSQLDeserialization implements DebeziumDeserializationSchema<String> {

    public static final String TS_MS = "ts_ms";
    public static final String DATABASE = "db";
    public static final String SCHEMA = "schema";
    public static final String TABLE = "table";
    public static final String BEFORE = "before";
    public static final String AFTER = "after";
    public static final String SOURCE = "source";

    private static final Long TIME_ZONE_UTC8 = 8 * 60 * 60 * 1000L;

    /**
     * 数据过滤器
     */
    private DataFilter dataFilter;

    public PostgreSQLDeserialization() {
        this.dataFilter = new DataFilter() {};
    }

    public PostgreSQLDeserialization(final DataFilter dataFilter) {
        this.dataFilter = dataFilter;
    }

    /**
     *
     * 反序列化数据,转为变更JSON对象
     * @param sourceRecord
     * @param collector
     * @return void
     * @author lei
     * @date 2022-08-25 14:44:31
     */
    @Override
    public void deserialize(SourceRecord sourceRecord, Collector<String> collector) {
        final String topic = sourceRecord.topic();
        log.debug("收到{}的消息，准备进行转换", topic);

        final DataChangeInfo dataChangeInfo = new DataChangeInfo();

        final Struct struct = (Struct) sourceRecord.value();
        final Struct source = struct.getStruct(SOURCE);
        dataChangeInfo.setBeforeData( getDataJsonString(struct, BEFORE));
        dataChangeInfo.setAfterData(getDataJsonString(struct, AFTER));

        //5.获取操作类型  CREATE UPDATE DELETE
        Envelope.Operation operation = Envelope.operationFor(sourceRecord);
        dataChangeInfo.setEventType(operation.toString().toLowerCase());
        dataChangeInfo.setDatabase(Optional.ofNullable(source.get(DATABASE)).map(Object::toString).orElse(""));
        dataChangeInfo.setSchema(Optional.ofNullable(source.get(SCHEMA)).map(Object::toString).orElse(""));
        dataChangeInfo.setTableName(Optional.ofNullable(source.get(TABLE)).map(Object::toString).orElse(""));
        dataChangeInfo.setChangeTime(Optional.ofNullable(struct.get(TS_MS)).map(x -> Long.parseLong(x.toString())).orElseGet(System::currentTimeMillis));

        // 添加逻辑表名
        if (SourceConfigWashMapper.ACTUAL_LOGICAL_TABLE_NAME.containsKey(dataChangeInfo.getTableName())) {
            dataChangeInfo.setLogicalTableName(
                    SourceConfigWashMapper.ACTUAL_LOGICAL_TABLE_NAME.get(dataChangeInfo.getTableName()));
        }

        if (dataFilter.filterOut(dataChangeInfo)) {
            return;
        }

        LogPrintLimitUtil.printDelayLog(log, String.format("收到%s的%s类型的消息， 已经转换好了，准备发往sink", topic, dataChangeInfo.getEventType()));
        //7.输出数据
        collector.collect(JsonUtils.toJSONString(dataChangeInfo));
    }

    private String getDataJsonString(final Struct struct, final String fieldName) {
        if (Objects.isNull(struct)) {
            return null;
        }
        final Struct element = struct.getStruct(fieldName);
        if (Objects.isNull(element)) {
            return null;
        }
        Map<String, Object> dataMap = new HashMap<>();
        Schema schema = element.schema();
        List<Field> fieldList = schema.fields();
        for (Field field : fieldList) {
            Object afterValue = element.get(field);
            if ("int64".equals(field.schema().type().getName())
                    && "io.debezium.time.MicroTimestamp".equals(field.schema().name())
                    && Objects.nonNull(afterValue)) {
                long times = (long) afterValue / 1000 - TIME_ZONE_UTC8;
                dataMap.put(field.name(), times);
            } else if ("int64".equals(field.schema().type().getName())
                    && "io.debezium.time.NanoTimestamp".equals(field.schema().name())
                    && Objects.nonNull(afterValue)) {
                long times = (long) afterValue - TIME_ZONE_UTC8;
                dataMap.put(field.name(), times);
            }  else if ("int64".equals(field.schema().type().getName())
                    && "io.debezium.time.Timestamp".equals(field.schema().name())
                    && Objects.nonNull(afterValue)) {
                long times = (long) afterValue - TIME_ZONE_UTC8;
                dataMap.put(field.name(), times);
            } else if("int32".equals(field.schema().type().getName())
                    && "io.debezium.time.Date".equals(field.schema().name())
                    && Objects.nonNull(afterValue)){
                int times = (int) afterValue;
                dataMap.put(field.name(), times * 24 * 60 * 60L * 1000);
            } else {
                dataMap.put(field.name(), afterValue);
            }
        }
        return JsonUtils.toJSONString(dataMap);
    }


    @Override
    public TypeInformation<String> getProducedType() {
        return TypeInformation.of(String.class);
    }
}
