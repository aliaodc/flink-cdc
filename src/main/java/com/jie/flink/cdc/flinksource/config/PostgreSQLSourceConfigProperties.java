package com.jie.flink.cdc.flinksource.config;

import com.jie.flink.cdc.service.PostgreSQLFlinkCdcService;
import lombok.Data;

import java.util.UUID;

/**
 * @author zhanggj
 * @data 2023/2/22
 */
@Data
public class PostgreSQLSourceConfigProperties extends SourceConfigProperties{

    public PostgreSQLSourceConfigProperties() {
        key = UUID.randomUUID().toString();
        sourceMap.put(key, new PostgreSQLFlinkCdcService(this));
    }

    /**
     * 库名
     */
    private String database;

    /**
     * schema 组
     */
    private String[] schemaArray;

    /**
     * 要监听的表
     */
    private String[] tableArray;

    /**
     * 重启后是否抛弃之前的checkPoint
     */
    private Boolean checkPointRenew;
}
