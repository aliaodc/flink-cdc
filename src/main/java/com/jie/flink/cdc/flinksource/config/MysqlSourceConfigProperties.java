package com.jie.flink.cdc.flinksource.config;

import com.jie.flink.cdc.service.MysqlFlinkCdcService;
import lombok.Data;

import java.util.UUID;

/**
 * @author zhanggj
 * @data 2023/2/22
 */
@Data
public class MysqlSourceConfigProperties extends SourceConfigProperties{

    public MysqlSourceConfigProperties() {
        key = UUID.randomUUID().toString();
        sourceMap.put(key, new MysqlFlinkCdcService(this));
    }
    /**
     * 库名
     */
    private String database;

    /**
     * 要监听的表
     */
    private String[] tableArray;

}
