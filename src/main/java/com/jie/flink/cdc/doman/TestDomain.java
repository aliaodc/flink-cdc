package com.jie.flink.cdc.doman;

import lombok.Data;

/**
 * @author zhanggj
 * @date 2023/5/5 17:58
 * @desc
 */
@Data
public class TestDomain {

    /**
     * 变更前数据
     */
    private String beforeData;
    /**
     * 变更后数据
     */
    private String afterData;
    /**
     * 变更类型 create=新增、update=修改、delete=删除、read=初始读
     */
    private String eventType;
    /**
     * 数据库名
     */
    private String database;
    /**
     * schema
     */
    private String schema;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 变更时间
     */
    private Long changeTime;


    private String slotName;
    private String plugin;
    private String slotType;
    private String datoid;
    private String temporary;
    private String active;
    private String activePid;
    private String xmin;
    private String catalogXmin;
    private String restartLsn;
    private String confirmedFlushLsn;
}
