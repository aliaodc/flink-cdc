package com.jie.flink.cdc.doman;


import lombok.Data;

import java.io.Serializable;

/**
 * @author zhanggj
 * @data 2023/1/31
 */
@Data
public class DataChangeInfo implements Serializable {

    /**
     * 变更前数据
     */
    private String beforeData;
    /**
     * 变更后数据
     */
    private String afterData;
    /**
     * 变更类型 create=新增、update=修改、delete=删除、read=初始读
     */
    private String eventType;
    /**
     * 数据库名
     */
    private String database;
    /**
     * schema
     */
    private String schema;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 逻辑表名
     * 针对分区（分表）表名
     * 例如 patrol_point[_0~1023] 逻辑表名为：patrol_point
     */
    private String logicalTableName;
    /**
     * 变更时间
     */
    private Long changeTime;
}
