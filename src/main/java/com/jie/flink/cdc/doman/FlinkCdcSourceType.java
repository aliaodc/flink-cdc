package com.jie.flink.cdc.doman;

import com.jie.flink.cdc.flinksource.config.MysqlSourceConfigProperties;
import com.jie.flink.cdc.flinksource.config.PostgreSQLSourceConfigProperties;
import com.jie.flink.cdc.flinksource.config.SourceConfigProperties;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.sql.Struct;

/**
 * @author zhanggj
 * @date 2023/5/25 14:09
 * @desc
 */
@Getter
public enum FlinkCdcSourceType {
    MYSQL("mysql", MysqlSourceConfigProperties.class),
    POSTGRESQL("postgresql", PostgreSQLSourceConfigProperties.class),

    ;

    private String code;
    private Class<? extends SourceConfigProperties> sourceConfigClass;

    FlinkCdcSourceType(String code, final Class<? extends SourceConfigProperties> sourceConfigClass) {
        this.code = code;
        this.sourceConfigClass = sourceConfigClass;
    }

    public static FlinkCdcSourceType getByCode(String code) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        for (FlinkCdcSourceType type : FlinkCdcSourceType.values()) {
            if (type.code.equals(code)) {
                return type;
            }
        }
        return null;
    }

}
