package com.jie.flink.cdc.doman;

import com.jie.flink.cdc.flinksink.config.SinkConfigProperties;
import com.jie.flink.cdc.flinksource.config.SourceConfigProperties;
import lombok.Data;

import java.util.List;

/**
 * @author zhanggj
 * @date 2023/5/25 11:09
 * @desc
 */
@Data
public class FlinkCdcStreamProperties {
    /**
     * 工作名称
     */
    private String jobName;

    private static final String SINK_PROPERTIES_KEY = "sinkConfigProperties";
    private List<SinkConfigProperties> sinkConfigList;

    private static final String SOURCE_PROPERTIES_KEY = "sourceConfigProperties";
    private SourceConfigProperties sourceConfigProperties;
}
