package com.jie.flink.cdc.doman;

import lombok.Data;

/**
 * @author zhanggj
 * @data 2023/2/15
 */
@Data
public class PgReplicationSlot {
    private String slotName;
    private String plugin;
    private String slotType;
    private String datoid;
    private String database;
    private String temporary;
    private String active;
    private String activePid;
    private String xmin;
    private String catalogXmin;
    private String restartLsn;
    private String confirmedFlushLsn;
}
