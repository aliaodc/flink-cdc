package com.jie.flink.cdc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhanggj
 * @data 2023/1/31
 */
@MapperScan("com.jie.flink.cdc.mapper")
@SpringBootApplication
public class OfflineDataApplication {
    public static void main(String[] args) {
        SpringApplication.run(OfflineDataApplication.class, args);
    }
}
