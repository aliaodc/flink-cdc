package com.jie.flink.cdc.mapper;

import com.jie.flink.cdc.doman.PgReplicationSlot;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhanggj
 * @data 2023/2/15
 */
public interface PgReplicationSlotMapper {
    List<PgReplicationSlot> selectAll();

    void deleteSlot(@Param("slotName") String slotName);
}
