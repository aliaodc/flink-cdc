package com.jie.flink.cdc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * @author zhanggj
 * @date 2023/5/31 17:23
 * @desc flink-steam 配置
 *jie:
 *   flink-cdc:
 *     flink-stream:
 *       stream2:
 *         source:
 *           type: postgresql
 *           url: jdbc:postgresql://127.0.0.1:5432/powerdb?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&currentSchema=power_work
 *           username: postgres
 *           password: jie.posgresql
 *           schemas: power_work
 *           tables: power_work.test_table
 *           initReadIgnore: true
 *           checkPointIgnore: true
 *         sink1:
 *           type: kafka
 *           bootstrapServers: kafka
 *           topic: kafka
 *         sink2:
 *           type: pulsar
 *           serviceUrl: pulsar://172.26.35.175:6650
 *           adminUrl: http://172.26.35.175:8080
 *           topic: persistent://youzhi-pulsar-tenant/namespace-youzhi/pulsar_test_01_topic
 *       stream3:
 *         source:
 *           type: mysql
 *           tables: mall.store_product_category
 *           url: jdbc:mysql://127.0.0.1:3306/mall?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8
 *           username: offline_data_user
 *           password: p^LMT*=QztQf
 *           initReadIgnore: true
 *         sink1:
 *           type: kafka
 *           bootstrapServers: kafka
 *           topic: kafka
 *         sink2:
 *           type: pulsar
 *           serviceUrl: pulsar://172.26.35.175:6650
 *           adminUrl: http://172.26.35.175:8080
 *           topic: persistent://youzhi-pulsar-tenant/namespace-youzhi/pulsar_test_01_topic
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "jie.flink-cdc")
public class FlinkCdcStreamConfigProperties {
    private Map<String, Map<String, Map<String, String>>> flinkStream;
}
