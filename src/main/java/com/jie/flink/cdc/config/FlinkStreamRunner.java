package com.jie.flink.cdc.config;

import com.jie.flink.cdc.service.FlinkCdcService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhanggj
 * @date 2023/5/25 10:51
 * @desc
 */
@Slf4j
@Order(10)
@Component
public class FlinkStreamRunner implements CommandLineRunner {
    @Resource(name = FlinkCdcStreamConfig.FLINK_CDC_SERVICE_LIST_NAME)
    private List<FlinkCdcService> flinkCdcServerList;

    @Override
    public void run(final String... args) throws Exception {
        if (CollectionUtils.isEmpty(flinkCdcServerList)) {
            return;
        }
        flinkCdcServerList.forEach(FlinkCdcService :: run);
    }
}
