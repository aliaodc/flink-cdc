package com.jie.flinkcdc.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;

/**
 * @author zhanggj
 * @data 2023/2/16
 */
public class UpdateTestTable {
    public static void main(String[] args) throws Exception {
        //注册驱动
        Class.forName("com.alibaba.druid.pool.DruidDataSource");
        //获取数据库连接
        Connection con= DriverManager.getConnection("jdbc:postgresql://localhost:5432/powerdb","postgres","jie.posgresql");
        //获取执行者对象
        Statement stat=con.createStatement();
        //执行sql语句并返回结果
        for (int i = 30; i < 90; i++) {
            String sql="update power_work.test_y_table set rr = 'time:" + i +
                    "'  where id = '111111111111111'";
            final int num = stat.executeUpdate(sql);

            System.out.println("update:" + i);
            TimeUnit.SECONDS.sleep(3);
        }
        //释放资源
        con.close();
    }
}
